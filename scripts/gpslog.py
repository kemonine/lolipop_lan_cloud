#!/usr/bin/env python3

OUTPUT_DIR='/tank/gps_data'

from datetime import datetime
import os
import shutil
import signal
import subprocess
import sys
import time

gpspipe1 = None
gpspipe1_file = None
gpspipe2 = None
gpspipe2_file = None

def signal_handler(signal, frame):
    # Kill running log gracefully or forcefully
    try:
        gpspipe1.terminate()
        if gpspipe1.poll() is None:
            gpspipe1.kill()

        # Kill running log gracefully or forcefully
        gpspipe2.terminate()
        if gpspipe2.poll() is None:
            gpspipe2.kill()
    except:
        pass

    # Cleanup any files that haven't been moved out of /tmp
    try:
        shutil.move(gpspipe1_file, os.path.join(OUTPUT_DIR, os.path.split(gpspipe1_file)[-1]))
    except:
        pass

    # Cleanup any files that haven't been moved out of /tmp
    try:
        shutil.move(gpspipe2_file, os.path.join(OUTPUT_DIR, os.path.split(gpspipe2_file)[-1]))
    except:
        pass
    sys.exit(0)

def gpspipe():
    filename = os.path.join('/', 'tmp', datetime.utcnow().strftime("%Y%m%d-%H%M") + '.nmea')
    return (filename,
            subprocess.Popen(['/usr/bin/gpspipe', '-r', '-l', '-o',
                             filename]
                            )
           )

# Handle as many signals as possible so `kill -BLAH` cleans up gpspipe process
#     as best as possible (KILL can't be trapped)
signal.signal(signal.SIGINT, signal_handler)
signal.signal(signal.SIGHUP, signal_handler)
signal.signal(signal.SIGQUIT, signal_handler)
signal.signal(signal.SIGTERM, signal_handler)
signal.signal(signal.SIGUSR1, signal_handler)
signal.signal(signal.SIGUSR2, signal_handler)
signal.signal(signal.SIGTSTP, signal_handler)

while True:
    if gpspipe1 is None:
        gpspipe1_file, gpspipe1 = gpspipe()

    time.sleep(600) # Wait 10 minutes

    gpspipe2_file, gpspipe2 = gpspipe()

    time.sleep(60) # Sleep 60 seconds (for overlap)

    # Kill running log gracefully or forcefully
    gpspipe1.terminate()
    if gpspipe1.poll() is None:
        gpspipe1.kill()

    shutil.move(gpspipe1_file, os.path.join(OUTPUT_DIR, os.path.split(gpspipe1_file)[-1]))

    gpspipe1 = gpspipe2
    gpspipe1_file = gpspipe2_file


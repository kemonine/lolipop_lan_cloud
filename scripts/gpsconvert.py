#!/usr/bin/env python3

from datetime import datetime
import glob
import os
import shutil
import subprocess
import time

# House keeping stuffs
DATA_DIR='/tank/gps_data'
OUT_DIR='/tank/gps_data/gpx'
PROCESSED_DIR='/tank/gps_data/raw'
OUT_FILE = os.path.join(OUT_DIR, datetime.utcnow().strftime('%Y%m%d-%H%M') + '.gpx')

# Build base gpsbabel conversion command
gpsbabelcmd = ['/usr/bin/gpsbabel', '-t', '-r', '-w', '-i', 'nmea']

# Get all non-processed GPS tracks
files = glob.glob(os.path.join(DATA_DIR,'*.nmea'))
files.sort()

# Add all files to conversion
for aFile in files:
    gpsbabelcmd.extend(['-f', aFile])

# Add final bits for conversion
gpsbabelcmd.extend(['-o', 'gpx', '-F', OUT_FILE])

# Call conversion (and wait for command to finish)
subprocess.check_call(gpsbabelcmd)

# Move files out of main directory so they aren't processed twice
for aFile in files:
    shutil.move(aFile, PROCESSED_DIR)

# Upload the converted GPX track to NextCloud for viewing with GpxPod
#apt install libxslt1-dev
#pip3 install webdavclient3
import webdav3.client as wc
options = {
 'webdav_hostname': 'https://nextcloud.domain.tld/remote.php/dav/files/gps',
 'webdav_login':    'gps',
 'webdav_password': 'gps1234',
 'webdav_verbose': True
}
client = wc.Client(options)
# Need to specify FULL file path for remote_path or SabreDAV will error
client.upload(remote_path='tracks/' + os.path.split(OUT_FILE)[-1], local_path=OUT_FILE)

# First Boot

Some things to do upon first boot

## Single Board Computers (Orange Pi, Raspberry Pi, etc)

If your SBC has only a single ethernet adapter you may want to run the following to help ease setup.

### Setup management LAN

The below steps and info should get you a VLAN based LAN that can be used for managing your lolipop if there are ever problems with WiFi. It will also lay the foundation for a 'proper' WiFi access point setup at a later point. The deleted WAN config will be reset later in the setup as well.

This procedure assumes you have a VLAN capable networking card in another computer. I leave you to figuring out how to setup a VLAN 😎

``` bash

uci batch <<EOF
set network.lan=interface
set network.lan.proto='static'
set network.lan.ipaddr='172.16.16.1'
set network.lan.netmask='255.255.255.0'
set network.lan.type='bridge'
set network.lan.ifname='eth0.16'
delete network.wan6
delete network.wan
EOF
uci commit

```

### Setup basic ethernet WAN support

The below will setup a new WAN interface using eth0 of the SBC. The reason for the VLAN above is so we can setup a basic WAN here for downloading packages and doing further fundamental setup.

``` bash

uci batch <<EOF
set network.wan=interface
set network.wan.proto='dhcp'
set network.wan.type='bridge'
set network.wan.ifname='eth0'
EOF
uci commit

```

### Verify settings

Verify the newly created interfaces have the appropriate setup with ```ip addr```. If all looks good you should be able to ```ping google.com``` successfully.

## Software

Upon first setup I always install the fundamental packages/software on my OpenWRT machines. Feel free to adjust the below lightly.

``` bash

opkg update
opkg install \
    wget nano pciutils usbutils rsync \
    htop tmux vim \
    zoneinfo-core \
    zoneinfo-africa zoneinfo-asia zoneinfo-atlantic zoneinfo-australia-nz \
    zoneinfo-europe zoneinfo-india zoneinfo-northamerica zoneinfo-pacific \
    zoneinfo-poles zoneinfo-southamerica \
    luci-ssl ca-certificates ca-bundle \
    luci-app-firewall luci-app-uhttpd luci-proto-ipv6 luci-proto-ppp \
    luci-app-ddns luci-app-openvpn luci-app-statistics luci-app-advanced-reboot \
    wpa-cli wpad wireless-tools wireless-regdb iw \
    swap-utils

```

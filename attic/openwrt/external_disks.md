# External Disks

Some basic notes on getting an external disk setup. Setting up the partition table and filesystem is left to the reader.

You probably want to use ext4 as the filesystem but I leave that final decision to you.

## Base Setup

Cribbed from [here](https://openwrt.org/docs/guide-user/storage/usb-drives)

``` bash

opkg install kmod-usb-storage kmod-usb-storage-extras block-mount kmod-scsi-generic usbutils kmod-usb-ehci kmod-usb-ohci libblkid blkid

```

## General Filesystems

``` bash

opkg install dosfstools kmod-fs-vfat kmod-fs-exfat

```

## Linux Filesystems

``` bash

opkg install kmod-fs-ext4 e2fsprogs f2fs-tools kmod-fs-f2fs

```

## Mac Filesystems

``` bash

opkg install hfsfsck kmod-fs-hfs kmod-fs-hfsplus

```

## Windows Filesystems

``` bash

opkg install ntfs-3g-utils ntfs-3g

```

## Setup for auto-mount

### Fundamentals and Find UUID

``` bash

mkdir /tank
blkid /dev/sda1

```

### LUCI Setup

- Open LUCI in your browser
- Navigate to  ```Mountpoints```
- Add the UUID with mountpoint /tank
- You may have some disappearing mount points, reboot if this happens

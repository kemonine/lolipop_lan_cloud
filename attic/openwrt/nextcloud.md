# NextCloud

## Credits

This section was inspired by [this guide (link)](https://openwrt.org/docs/guide-user/services/nas/owncloud). The info here has been updated to the latest release(s) of OpenWRT.

## Pre Flight

Follow the basic setup in [lighttpd.md](lighttpd.md) to get lighttpd installed and prepped for NextCloud. This documentation will adjust the base lighttpd install for serving NextCloud.

## Install Dependencies

``` bash

opkg update
opkg install diffutils patch
opkg install php7 php7-cli sqlite3-cli coreutils-stat ffmpeg \
    php7-mod-pcntl php7-mod-opcache \
    php7-cgi php7-fastcgi \
    php7-mod-exif php7-mod-gmp  \
    php7-mod-ctype php7-mod-dom php7-mod-gd \
    php7-mod-iconv php7-mod-json php7-mod-mbstring \
    php7-mod-simplexml php7-mod-xml \
    php7-mod-xmlreader php7-mod-xmlwriter \
    php7-mod-zip php7-mod-hash php7-mod-session \
    php7-mod-pdo php7-mod-pdo-sqlite php7-mod-sqlite3 \
    php7-mod-fileinfo php7-mod-intl php7-mod-openssl \
    php7-mod-curl curl \
    php7-mod-ldap php7-mod-ftp php7-mod-imap

```

## Setup lighttpd for PHP

``` bash

cat > /etc/lighttpd/conf.d/90-fastcgi-nextcloud.conf <<EOF
fastcgi.server = ( ".php" =>
                    ((
                    "host" => "127.0.0.1",
                    "port" => 1026,
                    "max-procs" => 4,
                    ))
                )
EOF
/etc/init.d/lighttpd restart

```

## Tune php.ini

Save the following patch to ```~/php.ini.patch``` and run ```patch < ~/php.ini.patch```

Cleanup any rejections by hand

``` diff

--- /etc/php.ini        2018-04-16 09:41:22.000000000 +0000
+++ /tank/examples/php.ini   2018-04-14 04:46:59.750973508 +0000
@@ -39,10 +39,10 @@

 ; Resource Limits

-max_execution_time = 30        ; Maximum execution time of each script, in seconds.
-max_input_time = 60    ; Maximum amount of time each script may spend parsing request data.
+max_execution_time = 300       ; Maximum execution time of each script, in seconds.
+max_input_time = 600   ; Maximum amount of time each script may spend parsing request data.
 ;max_input_nesting_level = 64
-memory_limit = 8M      ; Maximum amount of memory a script may consume.
+memory_limit = 16M     ; Maximum amount of memory a script may consume.

 ; Error handling and logging

@@ -82,7 +82,7 @@

 display_errors = On
 display_startup_errors = Off
-log_errors = Off
+log_errors = On
 log_errors_max_len = 1024
 ignore_repeated_errors = Off
 ignore_repeated_source = Off
@@ -95,7 +95,7 @@
 ;error_prepend_string = "<font color=#ff0000>"
 ;error_append_string = "</font>"
 ; Log errors to specified file.
-;error_log = /var/log/php_errors.log
+error_log = /var/log/php_errors.log
 ; Log errors to syslog.
 ;error_log = syslog

@@ -109,7 +109,7 @@
 register_long_arrays = Off
 register_argc_argv = On
 auto_globals_jit = On
-post_max_size = 8M
+post_max_size = 256M
 ;magic_quotes_gpc = Off
 magic_quotes_runtime = Off
 magic_quotes_sybase = Off
@@ -123,7 +123,7 @@

 ; UNIX: "/path1:/path2"
 ;include_path = ".:/php/includes"
-doc_root = "/www"
+doc_root = "/nextcloud"
 user_dir =
 extension_dir = "/usr/lib/php"
 enable_dl = On
@@ -139,7 +139,7 @@

 file_uploads = On
 upload_tmp_dir = "/tmp"
-upload_max_filesize = 2M
+upload_max_filesize = 512M
 max_file_uploads = 20

 ; Fopen wrappers
@@ -148,5 +148,13 @@
 allow_url_include = Off
 ;from="john@doe.com"
 ;user_agent="PHP"
-default_socket_timeout = 60
+default_socket_timeout = 600
 ;auto_detect_line_endings = Off
+
+opcache.enable=1
+opcache.enable_cli=1
+opcache.interned_strings_buffer=8
+opcache.max_accelerated_files=10000
+opcache.memory_consumption=128
+opcache.save_comments=1
+opcache.revalidate_freq=1

```

## Setup PHP-FastCGI

``` bash

/etc/init.d/php7-fastcgi start
/etc/init.d/php7-fastcgi enable

```

## Setup NextCloud Data Directory and Installer

``` bash

mkdir -p /tank/nextcloud
chown http /tank/nextcloud
mkdir -p /tank/tmp
chmod 777 /tank/tmp
mkdir /nextcloud
chown http /nextcloud
cd /nextcloud
wget https://download.nextcloud.com/server/installer/setup-nextcloud.php

```

## Base Install of NextCloud

- Navigate to ```https://172.16.16.1/setup-nextcloud.php```
- Enter ```.``` for the install directory
- Follow the prompts
- **AT THE FINAL STEP DO /NOT/ CLICK FINISH

## Prep NextCloud for final configuration/deployment

``` bash

chown -R http /nextcloud
cat > /nextcloud/config/config.php <<EOF
<?php
\$CONFIG = array (
  'datadirectory' => '/tank/nextcloud',
  'dbtype' => 'sqlite',
  'dbname' => 'nextcloud',
  'tempdirectory' => '/tank/tmp',
  'trusted_domains' =>
  array (
    0 => '172.16.16.1',
    1 => 'domain.tld'
  ),
  'defaultapp' => 'activity',
);
EOF
chown http /nextcloud/config/config.php

```

## Ensure any NextCloud config/setup is retained after sysupgrade

``` bash

cat >> /etc/sysupgrade.conf <<EOF
/etc/lighttpd/
/etc/php.ini
/nextcloud/
EOF

```

## Final Setup of NextCloud

- Navigate to ```https://172.16.16.1```
- Follow on-screen prompts for finalizing the NextCloud config
- Login as Admin

## Post Install

### Update/Install/Enable Apps

- Update any apps that are showing as out of date
- Enable Auditing / Logging app
- Enable Default encryption module
- Enable external storage support
- Install Group folders app
- Install External sites app
- Install Annoucement center app
- Enable Calendar Apps
- Enable Notes app
- Enable Tasks app
- Enable bookmarks app
- Enable brute force settings app
- Enable restrict login to IP addresses app
- Enable Two Factor TOTP Provider app
- Enable Two Factor U2F app
- Enable circles app
- Enable Impersonate app

### Basic Setup

#### Add Cronjob

Set the type of ```Background jobs``` to ```Webcron``` in ```Basic Settings```

``` bash

crontab -e
# Add
*/30  *  *  *  * wget https://127.0.0.1/cron.php --no-check-certificate -O -

```

#### Adjust Sharing settings

- Disable ```Allow public uploads```
- Disable ```Allow users on this server to send shares to other servers```
- Disable ```Send password by mail```

#### Adjust Security settings

Recommended Settings (Up to you)

- Minimal Length : 12
- Forbid common passwords
- Enforce upper and lower case characters
- Enforce numeric characters

### Setup Apps

- Setup file encryption : [https://docs.nextcloud.com/server/13/admin_manual/configuration_files/encryption_configuration.html](https://docs.nextcloud.com/server/13/admin_manual/configuration_files/encryption_configuration.html)
- Setup external sites app
  - Syncthing site that points to ```https://domain.tld:8384```
  - LUCI site that points to ```https://domain.tld:7443```
- Setup remaining apps from above

### Configure groups (as appropriate)

- Create group for standard users
- Create group folder for the new group (non-syncthing dumping ground for sync)
- Setup shared contacts list for new group
- Setup shared calendar for new group

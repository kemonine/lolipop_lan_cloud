# Base Setup

## Basic Security

- Setup a password via LUCI (this will also be used for SSH)
- Setup SSH to only run on the LAN network
- Setup uhttpd to only listen on the LAN interface
  - *BOTH* http and https

## LUCI General

After first boot you'll want to login to LUCI and tune the following

- Router hostname
- DHCP/DNS server setup for local domain
  - *Including* the local domain for the DHCP server (change the value from ```lan``` to ```domain.tld``` if you have your own domain
- Add lolipop.domain.tld under hostnames

## Swap

Most services prefer just a touch of swap, don't make it a lot but 'just enough'

### Create swap file

``` bash

mkdir /swap
dd if=/dev/zero of=/swap/swap.1 bs=1024000 count=64 # 64Mb
mkswap /swap/swap.1

```

### Activate swap via LUCI

- Open LUCI in your browser
- Navigate to ```Mountpoints```
- In the ```Swap``` section add swap device ```/swap/swap.1```
- Enable the newly created swap device
- Reboot the board if it remounts ```/``` read-only or throws an error in the LUCI UI

### Ensure swap is retained during sysupgrade

``` bash

cat >> /etc/sysupgrade.conf <<EOF
/swap/
EOF

```

## Custom poweroff Command

Most SBC's have power off functionality. You can run ```poweroff``` at the command line to turn the board off. Unfortnately the LUCI UI is missing this feature as of 2018/04/15. You can use the ```Custom Commands``` LUCI package to enable this feature.

### Install Dependencies

``` bash

opkg update
opkg install luci-app-commands

```

### Setup LUCI For Shudown

If you can find ```luci-app-advanced-reboot``` in the list of available packages. Install it instead of doing the below procedure.

- Open LUCI in your browser
- Navigate to ```Custom Commands``` section
- Add a new command
- Enter ```Turn off SBC/board``` as the description (or something similar)
- Enter ```poweroff``` as the command

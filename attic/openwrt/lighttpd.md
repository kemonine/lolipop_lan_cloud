# lighttpd

**Note:** lighttpd will have SSL support out of the box. See [letsencrypt.md](letsencrypt.md) for details on how to use Let's Encrypt to generate a valid SSL certificate.

## Prerequisites / Dependencies

``` bash

opkg update
opkg install lighttpd \
    lighttpd-mod-cgi lighttpd-mod-fastcgi \ # LUCI/NextCloud/PHP CGI needs
    lighttpd-mod-accesslog \ # Nice to have for troubleshooting
    lighttpd-mod-access \ # For access rules
    lighttpd-mod-alias \ # Nice to have
    lighttpd-mod-proxy \ # For reverse proxy to syncthing GUI
    lighttpd-mod-redirect \ # For redirecting all requests to SSL
    lighttpd-mod-rewrite \ # For cleaning up NextCloud URLs
    lighttpd-mod-setenv # For SSL related headers

```

## uhttpd REQUIRED Config Updates

- Open LUCI in your web browser
- Navigate to ```Services -> uhttpd```
- Set the ```http port``` to 7080
- Set the ```https port``` to 7443
- Restart uhttpd
- Login to LUCI on port 7080 or 7443 as appropriate

## Setup lighttpd for SSL module

Only load the module one time and leave it off other server/service configs.

```bash

cat > /etc/lighttpd/conf.d/50-ssl.conf <<EOF
server.modules += ( "mod_openssl" )
EOF
/etc/init.d/lighttpd restart

```

## Setup lighttpd for NextCloud

```bash

cat > /etc/lighttpd/conf.d/90-nextcloud.conf <<EOF

# SSL options
# server.modules += ( "mod_openssl" )
# server.modules += ( "mod_setenv" )
# server.modules += ( "mod_redirect" )
\$SERVER["socket"] == ":443" {
    server.document-root = "/nextcloud"

    ssl.engine              = "enable"
    ssl.pemfile             = "/etc/acme/domain.tld/domain.tld.combined.pem"
    ssl.ca-file             = "/etc/acme/domain.tld/ca.cer"
    ssl.honor-cipher-order  = "enable"
    # The following is OPTIONAL
    #ssl.cipher-list         = "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH:ECDHE-RSA-AES128-GCM-SHA384:ECDHE-RSA-AES12
    #ssl.use-compression     = "disable"
    setenv.set-response-header = (
        "X-Frame-Options" => "SAMEORIGIN",
        "X-Content-Type-Options" => "nosniff",
        "Strict-Transport-Security" => "max-age=15768000; includeSubdomains"
    )
    ssl.use-sslv2           = "disable"
    ssl.use-sslv3           = "disable"
}

# Redirect ALL web requests to https
\$HTTP["scheme"] == "http" {
    # capture vhost name with regex conditiona -> %0 in redirect pattern
    # must be the most inner block to the redirect rule
    \$HTTP["host"] =~ ".*" {
        url.redirect = (".*" => "https://%0\$0")
    }
}
EOF
/etc/init.d/lighttpd restart

```

## Proxy for Syncthing GUI

```bash

cat > /etc/lighttpd/conf.d/99-syncthing.conf <<EOF
# server.modules += ( "mod_openssl" )
# server.modules += ( "mod_setenv" )
# server.modules += ( "mod_proxy" )
\$SERVER["socket"] == "172.16.16.1:8384" {
    proxy.server = ( "" => ( ( "host" => "127.0.0.1", "port" => "8384" ) ) )
    proxy.header = ( "upgrade" => "enable" )

    # use <insecureSkipHostcheck>true</insecureSkipHostcheck> inside <gui> to get around host errors in an insecure fashion
    setenv.set-request-header = (
        "Host" => "127.0.0.1"
    )

    ssl.engine              = "enable"
    ssl.pemfile             = "/etc/acme/domain.tld/domain.tld.combined.pem"
    ssl.ca-file             = "/etc/acme/domain.tld/ca.cer"
    ssl.honor-cipher-order  = "enable"
    # The following is OPTIONAL
    #ssl.cipher-list         = "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH:ECDHE-RSA-AES128-GCM-SHA384:ECDHE-RSA-AES12
    #ssl.use-compression     = "disable"
    setenv.set-response-header = (
        "X-Frame-Options" => "DENY",
        "X-Content-Type-Options" => "nosniff",
        "Strict-Transport-Security" => "max-age=15768000; includeSubdomains"
    )
    ssl.use-sslv2           = "disable"
    ssl.use-sslv3           = "disable"
}
EOF

/etc/init.d/lighttpd restart

```

## Replace uhttpd for LUCI

```txt

THIS SECTION DOES **NOT** WORK!!!!

If you figure it out, please let me know!!!

```

```bash

opkg --force-depends remove uhttpd

cat > /etc/lighttpd/conf.d/90-luci.conf <<EOF
# SSL options
# server.modules += ( "mod_openssl" )
# server.modules += ( "mod_setenv" )
# server.modules += ( "mod_redirect" )
\$SERVER["socket"] == ":7443" {
    server.document-root = "/www"
    static-file.exclude-extensions = (".lua")
    cgi.assign = ( 
        #"luci" => "/usr/bin/lua",
        "cgi-bin/luci" => ""
        )

    # rewrites to keep luci code untouched
    #url.rewrite = ( "^/luci\$" => "/luci/", # helper only
    #                "^/cgi-bin/luci.*" => "/luci\$0",
    #                "^/luci-static/.*" => "/luci\$0" )

    ssl.engine              = "enable"
    ssl.pemfile             = "/etc/acme/domain.tld/domain.tld.combined.pem"
    ssl.ca-file             = "/etc/acme/domain.tld/ca.cer"
    ssl.honor-cipher-order  = "enable"
    # The following is OPTIONAL
    #ssl.cipher-list         = "EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH:ECDHE-RSA-AES128-GCM-SHA384:ECDHE-RSA-AES12
    #ssl.use-compression     = "disable"
    setenv.set-response-header = (
        "X-Frame-Options" => "DENY",
        "X-Content-Type-Options" => "nosniff",
        "Strict-Transport-Security" => "max-age=15768000; includeSubdomains"
    )
    ssl.use-sslv2           = "disable"
    ssl.use-sslv3           = "disable"
}

EOF

/etc/init.d/lighttpd restart

```

## Ensure any config gets retained during sysupgrad

``` bash

cat >> /etc/sysupgrade.conf <<EOF
/etc/lighttpd/
EOF

```

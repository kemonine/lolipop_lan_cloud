# Upgrading To New Release

## Inspired By / Additional Notes

- [https://github.com/lede-project/source/blob/master/package/base-files/files/etc/sysupgrade.conf](https://github.com/lede-project/source/blob/master/package/base-files/files/etc/sysupgrade.conf)
- [https://wiki.openwrt.org/doc/techref/sysupgrade](https://wiki.openwrt.org/doc/techref/sysupgrade)

## Critical Note

Be sure to add anything important to ```/etc/sysupgrade.conf```. Without entries in this file, whole sections of ```/``` and ```/etc``` may be excluded on an upgrade. There are examples throughout the documentation

## Prep for Upgrade

**Note:** This assumes ```/tank``` exists and is setup as an external disk. You're on your own if this is not the case.

``` bash

opkg update
opkg install swap-utils
opkg install block-mount
opkg install ca-bundle ca-certificates libustream-openssl
mkdir -p /tank/sysupgrade
cd /tank/sysupgrade
dd if=/dev/zero of=./swap.sysupgrade bs=1024000 count=512
mkswap ./swap.sysupgrade
swapon ./swap.sysupgrade
mount -o remount,size=512M /tmp

```

## Backup / Download / Base Image Upgrade

``` bash

CUR_DATE=`date +%Y%m%d-%H%M`
opkg list-installed | cut -f1 -d' ' > packages-$CUR_DATE.txt
wget https://downloads.openwrt.org/snapshots/targets/x86/64/openwrt-x86-64-combined-ext4.img.gz -O /tank/sysupgrade/openwrt-x86-64-combined-ext4-$CUR_DATE.img.gz
gunzip openwrt-x86-64-combined-ext4-$CUR_DATE.img.gz
sysupgrade -b /tank/sysupgrade/backup-$CUR_DATE.tar.gz
sysupgrade -v -p openwrt-x86-64-combined-ext4-$CUR_DATE.img

```

## Upgrade Packages

``` bash

opkg update
opkg list-upgradable | cut -f1 -d' ' | xargs opkg upgrade
reboot

```

## Restore Packages / Configs

``` bash

cd /tank/sysupgrade
INSTALLED_PKGS=`ls packages-*.txt | sort -r | head -n1`
opkg update
cat $INSTALLED_PKGS | xargs opkg install
LATEST_BACKUP=`ls backup*.tar.gz | sort -r | head -n1`
sysupgrade -r $LATEST_BACKUP
reboot

 ```

# Backups

Some quick and dirty details on how to setup robust backups.

This assumes an [external disk](external_disks.md) has been setup and is mounted at ```/tank```

## rsync

``` bash

opkg install rsync
mkdir /tank/backup
rsync -aPr --delete-after / /tank/backup/ --exclude=/tank --exclude=/proc --exclude=/sys --exclude=/tmp --exclude=/dev

```

### rsync Custom Command

One thing to consider is setting up the above rsync command as a 'Custom Command' in LUCI. The ```Custom poweroff Command``` section of [base_setup.md](base_setup.md) has a good example.

# Borg Backup

Borg backup is not currently supported on OpenWRT. It can likely be made to work on X86-64 but it's a no go for arm as of 2018/04/15.

Further reading:

- [https://borg.bauerj.eu/](https://borg.bauerj.eu/)
- [https://github.com/borgbackup/borg/issues/1018](https://github.com/borgbackup/borg/issues/1018)

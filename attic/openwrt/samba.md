# Samba (Basic File Sharing)

## Inspiration for this section came from

- [https://openwrt.org/docs/guide-user/services/nas/samba](https://openwrt.org/docs/guide-user/services/nas/samba)
- [https://openwrt.org/docs/guide-user/services/nas/usb-storage-samba-webinterface](https://openwrt.org/docs/guide-user/services/nas/usb-storage-samba-webinterface)

## Dependencies

This assumes you have plenty of storage on the root filesystem (```/```) or an external disk mounted at ```/tank```.

``` bash

opkg update
opkg install luci-app-samba
mkdir /tank
mkdir /tank/scratch
chmod 777 /tank/scratch # A generic dumping ground that's wide open (optional)

```

## LUCI (Setup samba)

- Navigate to ```Services -> Network Shares```
- Tweak the ```Hostname```. You probably want lolipop.domain.tld here
- Tweak the ```Description``` if desired
- Tweak the ```Workgroup``` if desired. I recommend domain.tld
- Turn off ```Allow system users to reach their home directories via network shares```
- Create system users for access (see below)
- Add a new ```Shared Directory``` for ```/tank/scratch```
  - ```Name : 'scratch'```
  - ```Path : '/tank/scratch'```
  - ```Browseable : 'Checked'```
  - ```Allow guests : 'Checked'```
- Add a new ```Shared Directory``` for ```/tank``` if desired
  - ```Name : 'tank'```
  - ```Path : '/tank'```
  - ```Allowed Users : '[comma separated list of users]'```
  - ```Browseable : 'Checked'```
  - ```Allow guests : 'Unchecked'```

## Example System user Creation

```bash

opkg update
opkg install shadow-groupadd shadow-useradd
useradd -s /bin/false auser
passwd auser

```

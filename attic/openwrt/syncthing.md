# Syncthing

## Download/Install Latest Release

``` bash

wget [https://github.com/syncthing/syncthing/releases]
tar -zxf syncthing-[version info].tar.gz
cp syncthing[version info]/syncthing /usr/bin
chmod a+x /usr/bin/syncthing
opkg update
opkg install shadow-useradd
useradd -m -d /syncthing -s /bin/false syncthing

```

## Prep a default, shared global folder

``` bash

mkdir -p /tank/syncthing/global
chown -R syncthing /tank/syncthing

```

## Configure / Setup Syncthing

**Note:** you may be warned about running syncthing as root. I leave creating a new user account for syncthing and updating the init script as an exercise to the reader. If you trust syncthing you can probably ignore this warning.

```bash

mkdir -p /syncthing
cat > /syncthing/config.xml <<EOF
<configuration version="28">
    <gui enabled="true" tls="false" debugging="false">
        <address>127.0.0.1:8384</address>
    </gui>
    <options>
        <listenAddress>172.16.16.1</listenAddress>
        <globalAnnounceEnabled>false</globalAnnounceEnabled>
        <localAnnounceEnabled>true</localAnnounceEnabled>
        <relaysEnabled>false</relaysEnabled>
        <natEnabled>false</natEnabled>
        <minHomeDiskFree unit="%">10</minHomeDiskFree>
        <defaultFolderPath>/tank/syncthing</defaultFolderPath>
    </options>
</configuration>
EOF
chown -R syncthing /syncthing

```

## Startup Script (init.d)

Cribbed from [here](https://github.com/brglng/syncthing-openwrt)

``` bash

cat > /etc/init.d/syncthing <<EOF
#!/bin/sh /etc/rc.common
# Copyright (C) 2015 brglng@github.com

SERVICE_UID=\`cat /etc/passwd | grep syncthing | cut -f3 -d':'\`

START=99
STOP=99

start() {
service_start /usr/bin/syncthing -home=/syncthing -logfile=/syncthing/syncthing.log 2>&1 | logger -t syncthing &
}

stop() {
service_stop /usr/bin/syncthing
}
EOF

chmod a+x /etc/init.d/syncthing
/etc/init.d/syncthing enable
/etc/init.d/syncthing start

```

## Ensure setup/files are retained after sysupgrade

``` bash

cat >> /etc/sysupgrade.conf <<EOF
/usr/bin/syncthing
/etc/init.d/syncthing
/syncthing/
EOF

```

## Finish Configuration via GUI

- ssh router with 8384 port forward
  - ```ssh -L8384:127.0.0.1:8384 root@172.16.16.1```
- Open ```https://127.0.0.1:8384``` in your web browser
- Configure ```/tank/syncthing/global``` as default shared folder
- Set ```Minimum disk space``` to ```10%```
- Disable ```Anonymous usage reporting```
- Setup a ```GUI Authentication User``` and ```GUI Authentication Password``` if you will be setting up a reverse proxy for the GUI via lighttpd

## Relaying

The author recommends *disabling* relay support for all clients and the deployed SBC. However, if you'd like to run your own relay server. The below links will be helpful. I leave this aspect of syncthing to you, good reader.

- [https://github.com/syncthing/relaysrv/releases](https://github.com/syncthing/relaysrv/releases)
- [https://docs.syncthing.net/users/strelaysrv.html](https://docs.syncthing.net/users/strelaysrv.html)

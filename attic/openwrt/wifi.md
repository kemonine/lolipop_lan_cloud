# WiFi Setup

The official OpenWRT/Armbian docs have good details on how to setup a WiFi access point and client mode(s). I'm not going to cover that here. Just some notes/polite reminders.

- See [docs/hardware_notes.md](hardware_notes.md) for details on USB hardware as well as the OpenWRT/LEDE/Armbian documentation
- Set a large, long password for any Access Points you setup
- Use separate WiFi adapters for AP mode and connecting to remote access points for WAN purposes
- Setup both 2.4ghz and 5ghz access points if possible
  - Use 5ghz to get beter speeds and avoid pollution
  - Mind your channels, your AP can drown out other APs you're trying to connect to with a different adapter in client mode
- WPA2 Pre Shared Key should be sufficient for security purposes
  - WPA2 Enterprise is the 'safest' option but has a *lot* of additional setup and tuning necessary to be effective
- Turn **OFF** WiFi ESSID broadcasting if you want to remain more anonymous
  - This is not perfect but can help

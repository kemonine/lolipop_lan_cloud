# collectd / statistics

Some basic info / notes on getting good statistics

## Install Packages

``` bash

opkg update
opkg install collectd \
    collectd-mod-cpu \
    collectd-mod-interface \
    collectd-mod-iwinfo \
    collectd-mod-load collectd-mod-memory \
    collectd-mod-network collectd-mod-rrdtool \
    collectd-mod-conntrack \
    collectd-mod-cpufreq collectd-mod-df \
    collectd-mod-disk collectd-mod-iptables \
    collectd-mod-netlink \
    collectd-mod-ntpd \
    collectd-mod-openvpn \
    collectd-mod-rrdtool \
    collectd-mod-sensors \
    collectd-mod-thermal
/etc/init.d/collectd restart

```

## Configure

LUCI has a whole section titled ```Statistics``` at the top. Poke around.

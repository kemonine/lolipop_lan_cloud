# Adblocking (DNS Based)

Insipred by / credits : [https://github.com/openwrt/packages/blob/master/net/unbound/files/README.md](https://github.com/openwrt/packages/blob/master/net/unbound/files/README.md)

## Dependencies / Prereqs

``` bash

mkdir -p /tank/adblock
opkg update
opkg install adblock luci-app-adblock unbound luci-app-unbound
/etc/init.d/adblock enable
/etc/init.d/unbound enable

```

## Configure Adblock

Logic to LUCI and navigate to ```Services``` -> ```Adblock```

Verify the following settings

- ```Enable Adblock``` : ```Checked```
- ```DNS Backend (DNS Directory)``` : ```unbound (/var/lib/unbound)```
- ```Download Utility``` : ```uclient-fetch```
- ```Enable Blocklist Backup``` : ```Checked```
- ```Backup Directory``` : ```/tank/adblock```

Enable the following sources

- adaway
- adguard
- bitcoin
- blacklist
- disconnect
- feodo
- malware
- malwarelist
- openphish
- ransomware
- winspy
- yoyo
- zeus

## Setup DNS for Adblock

Logic to LUCI and navigate to ```Service``` -> ```Recursive DNS```

Under ```Basic`` verify the following

- ```Enable Unbound``` : ```Checked```
- ```Local Service``` : ```Checked```
- ```Enable DNSSEC``` : ```Checked```
- ```DNSSEC NTP Fix``` : ```Checked```
- ```Domain Insecure``` : ```test.domain.tld```
- ```Listening Port``` : ```1053```

Under ```Advanced``` verify the following

- ```DHCP Link``` : ```No Link```
- ```Local Domain``` : ```test.kemonine.info```
- ```Local Domain Type``` : ```Refused```
- ```LAN DNS``` : ```No Entry```
- ```WAN DNS``` : ```Use Upstream```
- ```Extra DNS``` : ```Ignore```
- ```Filter Localhost Rebind``` : ```Checked```
- ```Filter Private Rebind``` : ```Filter RFC 1918/4193```

Under ```Resource``` verify the following

- ```Trigger Networks``` : ```LAN ONLY```

## Setup DNS Forwarding for DHCP

Logic to LUCI and navigate to ```Network``` -> ```DHCP and DNS```

Under ```General Settings``` verify the following

- ```DNS forwardings```
  - ```127.0.0.1#1053```
  - ```::1#1053```
- ```Rebind Protection``` : ```Checked```
- ```Allow localhost``` : ```Checked```
- ```Local Service Only``` : ```Checked```
- ```Non-wildcard``` : ```Checked```

Under ```Resolv and Hosts Files``` verify the following

- ```Use /etc/ethers``` : ```Checked```
- ```Ignore resolve file``` : ```Checked```

# Let's Encrypt Certificates

## Dependencies

```bash

opkg install acme socat # NOTE LACK OF LUCI INTEGRATION DUE TO DNS

cd /usr/lib/acme
/usr/lib/acme/acme.sh --install
cd ~/.acme.sh
/usr/lib/acme/acme.sh --upgrade
cp -r * /usr/lib/acme

```

## Generate new certificate

The below is an example using CloudFlare DNS for verification

```bash

export CF_Email='[your cloudflare email]'
export CF_Key='[your cloudflare api key]'
/usr/lib/acme/acme.sh [--issue|renew] \
    --log /var/log/acme \
    --home /etc/acme \
    --accountemail user@domain.tld \
    --domain domain.tld --domain lolipop.domain.tld \
    --dns dns_cf \
    --staging

```

## Renewal Script

I leave creating a renewal script and setting up a cron job as an exercise to the reader. The above block of commands should be helpful ;)

## Convert certificate for uhttpd

```bash

cd /etc/acme/[domain.tld]
openssl rsa -in domain.tld.key -outform DER -out /etc/uhttpd.key
openssl x509 -in domain.tld.cer -outform DER -out /etc/uhttpd.crt
/etc/init.d/uhttpd restart

```

## Convert certificate for lighttpd

```bash

cd /etc/acme/[domain.tld]
cat domain.tld.cer domain.tld.key >> domain.tld.combined.pem

```

## Ensure necessary files are retained during sysupgrade

``` bash

cat >> /etc/sysupgrade.conf <<EOF
/etc/acme/
/etc/uhttpd.key
/etc/uhttpd.crt
/path/to/renew/script
EOF

```

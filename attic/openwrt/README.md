# OpenWRT

An approach and docs using just OpenWRT

## The Basics

- [Pre Flight](../preflight.md)
- [Hardware Notes](..//hardware_notes.md)
- [First Boot](first_boot.md)
- [Base Setup](base_setup.md)

## Storage

- [External Disks](external_disks.md)
- [Backups](backups.md)
- [Samba (basic file sharing)](samba.md)

## Web Server(s)

- [Let's Encrypt](letsencrypt.md)
- [lighttpd (web server)](lighttpd.md)

## Cloud Services

- [Syncthing](syncthing.md)
- [NextCloud](nextcloud.md)

## Misc

- [WiFi](wifi.md)
- [Upgrading](upgrading.md)
- [Private Internet Access (PIA)](pia.md)
- [AdBlock](adblock.md)
- [collectd/stats](collectd_stats.md)

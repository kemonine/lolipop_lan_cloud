https://wiki.qemu.org/Documentation

https://qemu.weilnetz.de/doc/qemu-doc.html

https://wiki.qemu.org/Documentation/Platforms/ARM

https://gist.github.com/catalsdevelop/4b50f53b786d6c5ac2a3273f3f723455


http://aleph0.info/software/lisp-forth-openwrt/openwrt-on-qemu.html

apt update
apt install qemu qemu-utils qemu-system-aarch64 qemu-system-arm

apt install qemu-system-mips


# https://downloads.openwrt.org/snapshots/targets/armvirt/
mkdir openwrt_qemu
cd openwrt_qemu
mkdir 32
mkdir 64
wget -O 32/root.ext4.gz https://downloads.openwrt.org/snapshots/targets/armvirt/32/openwrt-armvirt-32-root.ext4.gz
wget -O 32/zImage https://downloads.openwrt.org/snapshots/targets/armvirt/32/openwrt-armvirt-32-zImage
wget -O 32/zImage-initramfs https://downloads.openwrt.org/snapshots/targets/armvirt/32/openwrt-armvirt-32-zImage-initramfs
wget -O 64/Image https://downloads.openwrt.org/snapshots/targets/armvirt/64/openwrt-armvirt-64-Image
wget -O 64/Image-initramfs https://downloads.openwrt.org/snapshots/targets/armvirt/64/openwrt-armvirt-64-Image-initramfs
wget -O 64/root.ext4.gz https://downloads.openwrt.org/snapshots/targets/armvirt/64/openwrt-armvirt-64-root.ext4.gz

# -usbdevice host:vendor_id:product_id \

qemu-system-arm -name openwrt-arm32-test \
    -M vexpress-a15 -smp 1 -m 128M -nographic \
    -machine usb=on \
    -usbdevice host:0b05:17ab \
    -kernel zImage-initramfs \
    -sd root.ext4 \
    -append "console=ttyAMA0 verbose debug root=/dev/mmcblk0p1"


qemu-system-aarch64 -name openwrt-arm64-test \
    -M virt -smp 1 -m 256M -nographic \
    -usb \
    -kernel Image-initramfs \
    -sd root.ext4 \
    -append "console=ttyAMA0 verbose debug root=/dev/mmcblk0p1"




virt machine type

-drive "file=$IMG,format=raw,if=virtio" \
id=sd0,file=root.ext4,if=sd,bus=0,unit=0

-netdev bridge,id=wan,br="$BR_WAN,helper=$HELPER" -device pcnet,netdev=wan,mac="$MAC_WAN" \
-netdev bridge,id=lan,br="$BR_LAN,helper=$HELPER" -device pcnet,netdev=lan,mac="$MAC_LAN" \


qemu-system-aarch64 -m <memory size> -M <machine name> -drive if=none,file=<hard drive file name>,id=hd0 \
-device virtio-blk-device,drive=hd0 -netdev type=tap,id=net0 -device virtio-net-device,netdev=net0

qemu-system-arm -M realview-eb-mpcore -kernel openwrt-realview-vmlinux-initramfs.elf -net nic -net user -nographic

qemu-system-arm -M realview-pbx-a9 -m 1024M -nographic \
    -kernel openwrt-realview-vmlinux.elf \
    -sd openwrt-realview-sdcard.img \
    -append "console=ttyAMA0 verbose debug root=/dev/mmcblk0p1"

qemu-system-aarch64 -machine virt -cpu cortex-a57 -machine type=virt -nographic \
-smp 1 -m 2048 \
-kernel bin/arm64/openwrt-arm64-qemu-virt-initramfs.Image \
--append "console=ttyAMA0"

qemu-system-mipsel -kernel openwrt-malta-le-vmlinux-initramfs.elf -nographic -m 256

qemu-system-mipsel -M malta \
-hda openwrt-malta-le-root.ext4 \
-kernel openwrt-malta-le-vmlinux.elf \
-nographic -append "root=/dev/sda console=ttyS0"

$ qemu-system-mipsel -kernel my-openwrt-image.elf M malta \
                     -hda openwrt-malta-le-root.ext4 \
                     -append "root=/dev/sda console=ttyS0" \
                     -nographic
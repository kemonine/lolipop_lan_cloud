https://github.com/nmaas87/docker-openwrt

https://github.com/nmaas87/docker-openwrt/blob/master/trunk_rpi3/Dockerfile

https://www.armbian.com/

https://hub.docker.com/r/diginc/pi-hole/

https://hub.docker.com/r/diginc/pi-hole-multiarch/tags/

https://github.com/nextcloud/docker

https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion

https://github.com/Drakulix/armhf-syncthing/blob/master/Dockerfile

https://docs.docker.com/engine/reference/run/#runtime-constraints-on-resources

https://blog.alexellis.io/docker-orangepi/

https://wiki.openwrt.org/doc/howto/docker_openwrt_image

https://github.com/samm-git/turris-containers/blob/master/lxc-templates/lxc-turrisos

https://stgraber.org/2017/03/27/usb-hotplug-with-lxd-containers/

apt update
apt upgrade
systemctl reboot
curl -sSL https://get.docker.com | sh


uci batch <<EOF
set network.lan.proto='static'
set network.lan.ipaddr='172.16.16.1'
set network.lan.netmask='255.255.255.0'
set network.lan.type='bridge'
set network.lan.ifname='eth0.16'
delete network.wan6
delete network.wan
EOF
uci commit



uci batch <<EOF
set network.WAN=interface
set network.WAN.proto='dhcp'
set network.WAN.type='bridge'
set network.WAN.ifname='eth0'
EOF
uci commit


opkg update
opkg install \
    wget nano pciutils usbutils rsync \
    htop tmux vim \
    zoneinfo-core \
    zoneinfo-africa zoneinfo-asia zoneinfo-atlantic zoneinfo-australia-nz \
    zoneinfo-europe zoneinfo-india zoneinfo-northamerica zoneinfo-pacific \
    zoneinfo-poles zoneinfo-southamerica \
    luci-ssl ca-certificates ca-bundle \
    luci-app-firewall luci-app-uhttpd luci-proto-ipv6 luci-proto-ppp \
    luci-app-ddns luci-app-openvpn luci-app-statistics luci-app-advanced-reboot \
    wpa-cli wpa-supplicant hostapd hostapd-utils \
    swap-utils


docker build -t openwrt:test-1 .

docker run \
    --memory="128M" \
    --memory-swap="-1" \
    --cpus=1 \
    --tty \
    --interactive \
    --init \
    --privileged \
    --name "OpenWRT-Test" \
    openwrt:test-1


--network="host" \
--privileged \
--init \

docker exec -ti OpenWRT-Test /bin/ash


docker rm OpenWRT-Test

docker logs OpenWRT-Test -f


wget https://downloads.openwrt.org/snapshots/targets/sunxi/cortexa53/openwrt-sunxi-cortexa53-sun50i-a64-pine64-plus-ext4-sdcard.img.gz
gunzip openwrt-sunxi-cortexa53-sun50i-a64-pine64-plus-ext4-sdcard.img.gz
mkdir img
mount openwrt-sunxi-cortexa53*.img img
ls img/


https://downloads.openwrt.org/snapshots/targets/armvirt/64/openwrt-armvirt-64-root.ext4.gz
mount openwrt-armvirt-64-root.ext4 img/
tar -C /mnt/openwrt --numeric-owner --exclude=/proc --exclude=/sys -cvf openwrt.tar .



https://github.com/lxc/lxd/blob/master/doc/containers.md?utf8=%E2%9C%93

https://stgraber.org/2016/03/30/lxd-2-0-image-management-512/

https://stgraber.org/2017/03/27/usb-hotplug-with-lxd-containers/

apt update
apt install lxc lxc-templates lxd lxd-client lxd-tools
apt install cloud-utils debootstrap

apt install lxc lxctl

systemctl disable hostapd
systemctl disable wpa_supplicant
systemctl disable NetworkManager

# AppArmor prevents standard Ubuntu LXC containers from starting...
apt install apparmor-utils
#sysmtctl stop apparmor
#systemctl disable apparmor
#systemctl reboot

# Verify LXC is working on a basic level
lxc-create --template ubuntu --name ubnt-test -- --arch arm64 --release xenial
lxc-start -F --name ubnt-test
lxc-stop --name ubnt-test
lxc-destroy --name ubnt-test


# Prep OpenWRT (armvirt-64) base image

Note : the qemu images are what we want for arm32/64/else

Adjust the wget for the generic arm (Orange Pi / Pine64) or for the rPi specific images (Pi 1/2/3/zero/etc)

wget https://downloads.openwrt.org/snapshots/targets/armvirt/64/openwrt-armvirt-64-default-rootfs.tar.gz

mkdir -pv /var/lock && mkdir -pv /var/run

lxc-create -n openwrt-test-new -t /tank/lxc/salt_tarball -- --imgtar /tank/lxc/openwrt-armvirt-64-default-rootfs.tar.gz
lxc-ls
lxc-info --name openwrt-test
lxc-start --name openwrt-test-new-3 \
    --logfile /var/log/lxc/openwrt-test-debug.log \
    --logpriority DEBUG \
    -F
lxc-console --name openwrt-test-lan
lxc-execute --name openwrt-test /bin/ash

If you see ```'Generating a client certificate.``` be patient and/or run [h]top in a separate console. It can take a long while on SBCs.

lxc image import \
    ./metadata.tar \
    ./openwrt-armvirt-64-default-rootfs.tar.gz \
    --alias openwrt-armvirt-64-2
lxc image list
lxc image alias list

lxc launch openwrt-armvirt-64 openwrt-test-2

lxc-create --name openwrt-test-lan \
    --template /root/lxc-openwrt-lan

lxc-start --name openwrt-test-lan \
    --logfile /var/log/lxc/openwrt-test-debug.log \
    --logpriority DEBUG \
    -F


disable ubuntu handling of eth0
pre-patch openwrt network configto handle lan/wan properly
zap etc/hotplug-preinit.json contents except for [] # this is already done by host ubuntu




wget https://downloads.openwrt.org/snapshots/targets/sunxi/cortexa53/openwrt-sunxi-cortexa53-sun50i-a64-pine64-plus-ext4-sdcard.img.gz

gunzip openwrt-sunxi-cortexa53-sun50i-a64-pine64-plus-ext4-sdcard.img.gz
23068672
fdisk -lu *.img
Sector size * Start = offset (512*45056 = 23068672)
losetup -o 23068672 /dev/loop0 openwrt-sunxi-cortexa53-sun50i-a64-pine64-plus-ext4-sdcard.img
mkdir rootfs
mount /dev/loop0 rootfs
tar -cf ./openwrt-sunxi-cortexa53-sun50i-a64-pine64-plus-rootfs.tar rootfs/*

lxc-create --name openwrt-test-pine64 \
    --template /root/lxc-openwrt-pine64

lxc-start --name openwrt-test-lan \
    --logfile /var/log/lxc/openwrt-test-debug.log \
    --logpriority DEBUG \
    -F


lxc-create --name openwrt-test \
    --template /root/lxc-openwrt

lxc-start --name openwrt-test \
    --logfile /var/log/lxc/openwrt-test-debug.log \
    --logpriority DEBUG \
    -F


#lxc.network.type = none
lxc.aa_profile = unconfined
security.privileged = true
#lxc.cap.drop=



cat > /etc/config/wireless <<EOF
config 'wifi-device' 'wlx08606ed02041'
EOF

lsusb
Bus 003 Device 002: ID 0b05:17ab ASUSTek Computer, Inc. USB-N13 802.11n Network Adapter (rev. B1) [Realtek RTL8192CU]
lxc config device add openwrt wifi0 usb vendorid=0b05 productid=17ab


root@pine64:/var/lib/lxc/openwrt-test# cat metadata.yaml
architecture: "arm64"
creation_date: 1458040200
properties:
 architecture: "arm64"
 description: "OpenWRT arm64 snapshot"
 os: "openwrt"
 release: "snapshot"

lxc image import metadata.tar rootfs.tar --alias openwrt-arm64-snapshot



root@pine64:/var/lib/lxc/openwrt-test# lxc profile show openwrt
config:
  environment.http_proxy: http://[fe80::1%eth0]:13128 # Remove me?
  limits.cpu: "1"
  limits.memory: 128MB
  raw.apparmor: unconfined
  raw.lxc: |-
    lxc.haltsignal = SIGUSR1
    lxc.mount.entry = proc proc proc nodev,noexec,nosuid 0 0
    lxc.mount.entry = sysfs sys sysfs defaults 0 0
    lxc.mount.entry = tmpfs tmp tmpfs defaults
    lxc.include = /usr/share/lxc/config/openwrt.common.conf
  security.privileged: "true"
  user.network_mode: link-local
description: Default LXD profile
devices:
  eth0:
    name: eth0
    nictype: physical
    parent: eth0
    type: nic
name: openwrt
used_by: []





lxc launch openwrt-arm64-snapshot openwrt-test-lxc2 --profile openwrt
lxc start openwrt-test-lxc2 --debug
lxc delete openwrt-test-lxc2

lxc profile set openwrt raw.lxc \
"lxc.haltsignal = SIGUSR1
lxc.mount.entry = proc proc proc nodev,noexec,nosuid 0 0
lxc.mount.entry = sysfs sys sysfs defaults 0 0
lxc.mount.entry = tmpfs tmp tmpfs defaults
lxc.include = /usr/share/lxc/config/openwrt.common.conf"




/var/log/lxd/openwrt-test-lxc/lxc.log
/var/lib/lxd/containers/openwrt-test-lxc/rootfs

https://help.ubuntu.com/lts/serverguide/lxc.html
http://us.images.linuxcontainers.org/
https://discourse.pi-hole.net/t/hardware-software-requirements/273
https://github.com/pi-hole/pi-holehttps://github.com/pi-hole/pi-hole/wiki/Core-Function-Breakdown
https://discourse.pi-hole.net/t/the-pihole-command-with-examples/738
https://github.com/pi-hole/pi-hole/blob/master/automated%20install/basic-install.sh
https://stgraber.org/2016/03/26/lxd-2-0-resource-control-412/


# Add full tar tool so -J option works with lxc-create
# xz-utils, liblzma to ensure full xz stack is present for lxc-create (if needed)
opkg update
opkg install luci-app-lxc tar xz-utils liblzma \
lxc-autostart lxc-checkconfig lxc-attach lxc-console lxc-execute lxc-info lxc-monitor lxc-snapshot lxc-start lxc-stop lxc-create lxc-destroy 	lxc-cgroup lxc-configs lxc-copy lxc-device lxc-templates lxc-top lxc-unprivileged lxc-user-nic 


Services -> LXC Containers Tweaks
    Change Containers URL to http://images.linuxcontainers.org/

# no-validate because gpg isn't available on openwrt
lxc-create --name pihole --template download -- --no-validate --dist ubuntu --release xenial --arch arm64 --flush-cache

lxc-create --name piholetest --template download -- --no-validate --dist ubuntu --release xenial --arch amd64 --flush-cache

lxc-create --name piholetest2 --template download -- --no-validate --dist alpine --arch amd64


opkg install mount-utils
reboot
mkdir /sys/fs/cgroup
mount -t cgroup cgroup /sys/fs/cgroup


lxc-start -F -n piholetest -- --logprority DEBUG
lxc-attach -n piholetest

lxc-info -n piholetest


lxc-destroy -n piholetest


lxc profile list
lxc profile show default  -- copy that yaml
then lxc profile create ram-limited, hack on it, and boot one to verify it has the ram limits iniitated

# To Do

A bunch of stuff that really should be addressed and/or figured out. These items are loosly in order of priority for the author.

## Future

- Look into TiddlyWiki as alternative to NextCloud notes add-on
  - https://play.google.com/store/apps/details?id=de.mgsimon.android.andtidwiki&hl=en
  - https://tiddlywiki.com/static/GettingStarted%2520-%2520Android.html
  - https://tiddlywiki.com/

# License

All work contained within this repo is licensed [Creative Commons Attribution 4.0 International License (CC BY 4.0)](https://creativecommons.org/licenses/by/4.0/).

The full text of the license is also [here](LICENSE).

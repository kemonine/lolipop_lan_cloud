# Preflight

Some things you may or may not want to consider ahead of time

## Hardware

- Orange Pi R1
- Orange Pi Zero Plus
- Pine64 Plus
  - (see [hardware notes](hardware_notes.md) for additional considerations)
- Orange Pi Zero
  - (see [hardware notes](hardware_notes.md) for additional considerations)
- Raspberry Pi
  - (see [hardware notes](hardware_notes.md) for additional considerations)

### Offical Support

Be sure to check OpenWRT/Armbian for your chosen router/boards support status. A number are going to be officially supported while others will have development snapshots avilable.

## Domain

If you want to have an 'official' domain for your device, it's possible and this documentation will cover some of the setup.

### Registrars

A couple of good registrars for registering a domain

- [http://namecheap.com/](http://namecheap.com/)
- [https://uniregistry.com/](https://uniregistry.com/)

### TLDs (Top Level Domains)

Some good TLDs for a lolipop device

- .zone
- .host
- .travel
- .link
- .online
- .net
- .tech
- .club

### Domain Preflight

In your chosen registrar you'll need to setup API access if you want to use Let's Encrypt for SSL certificates (recommended). Check with your registrar for necessary information on setup.

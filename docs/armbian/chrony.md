# Chrony

Setup alternative ntp that does well with systems that may or may not always be online.

## Inspiration / Further Reading

- [https://wiki.archlinux.org/index.php/Chrony](https://wiki.archlinux.org/index.php/Chrony)
- [https://insights.ubuntu.com/2018/04/09/ubuntu-bionic-using-chrony-to-configure-ntp](https://insights.ubuntu.com/2018/04/09/ubuntu-bionic-using-chrony-to-configure-ntp)
- [http://manpages.ubuntu.com/manpages/trusty/man5/chrony.conf.5.html](http://manpages.ubuntu.com/manpages/trusty/man5/chrony.conf.5.html)

## Install

``` bash

apt update
apt install chrony
systemctl enable chrony # Enable service
systemctl start chrony # Start service
chronyc activity # Verify install successful
systemctl disable ntp.service # Disable std ntpd (replaced by chrony)

```

## Configuration

``` bash

cat >> /etc/chrony/chrony.conf <<EOF
allow 172.16.16.0/24
allow 172.17.17.0/24
allow 172.18.18.0/24
allow 172.30.0.0/16

# Allow large clock adjustments (you want this as there is no RTC on most SBCs)
makestep 1 -1
EOF

```

Run ```systemctl restart chrony``` to pickup the changes.

## Allow NTP access via internal/trusted networks

``` bash

firewall-cmd --permanent --zone=internal --add-service ntp
firewall-cmd --permanent --zone=trusted --add-service ntp
firewall-cmd --reload
firewall-cmd --info-zone internal
firewall-cmd --info-zone trusted

```

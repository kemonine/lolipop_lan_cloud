# Wallabag

Self hosted Read It Later / Pocket service. Read the net while offline.

## Inspiration / Further Reading


- [https://hub.docker.com/r/wallabag/wallabag/~/dockerfile/](https://hub.docker.com/r/wallabag/wallabag/~/dockerfile/)
- [https://github.com/wallabag/wallabag](https://github.com/wallabag/wallabag)

## Prep

``` bash

mkdir -p /var/wallabag/var /var/wallabag/data /var/wallabag/assets /var/wallabag/cache
mkdir -p /root/docker/wallabag
git clone https://gitlab.com/kemonine/wallabag-in-a-can.git /root/docker/wallabag/src

```

## Install / Setup

``` bash

cat > /root/docker/wallabag/wallabag.sh <<EOF
cd /root/docker/wallabag/src
git pull

VERSION=\`curl -s https://api.github.com/repos/wallabag/wallabag/releases/latest | jq -r .tag_name\`

ARCH=\`arch\`

# Cleanup arch/container image here
if [ \$ARCH == "aarch64" ]
then
    echo "64bit arm"
    ALPINE="arm64v8/alpine:latest"
else
    echo "32bit arm"
    ALPINE="arm32v6/alpine:latest"
fi

docker build \\
    --network docker-private \\
    --file ./Dockerfile \\
    --build-arg ALPINE=\$ALPINE \\
    --build-arg VERSION=\$VERSION \\
    --tag wallabag/wallabag:\$VERSION \\
    .

# Cleanup existing container
docker stop wallabag
docker rm wallabag

SECRET=\`dd if=/dev/urandom bs=128 count=1 | base64\`
SECRET=\`echo $SECRET | tr --delete '\n' | tr --delete ' '\`

##########
# For postgresql instead of sqlite run the following commands
#docker exec -it postgres psql -U postgres
#create role wallabag nocreatedb nocreaterole login PASSWORD 'password';
#create database wallabag owner=wallabag encoding=UTF8;

# Setup the below using the above database/username/role and ip of 172.30.12.12
# You'll also need to use pdo_pgsql instead of pdo_sqlite in the below env variables
##########

# Re-run/create container with latest image
docker run \\
    --name wallabag \\
    --restart unless-stopped \\
    --net docker-private \\
    --ip 172.30.9.9 \\
    -e TZ=UTC \\
    -e DEBUG=1 \\
    -e SYMFONY__ENV__DATABASE_DRIVER=pdo_sqlite \\
    -e SYMFONY__ENV__DATABASE_HOST=127.0.0.1 \\
    -e SYMFONY__ENV__DATABASE_PORT=null \\
    -e SYMFONY__ENV__DATABASE_PATH=/wallabag/data/db/wallabag.sqlite \\
    -e SYMFONY__ENV__DATABASE_NAME=wallabag \\
    -e SYMFONY__ENV__DATABASE_USER=null \\
    -e SYMFONY__ENV__DATABASE_PASSWORD=null \\
    -e SYMFONY__ENV__SECRET="\$SECRET" \\
    -e SYMFONY__ENV__DOMAIN_NAME="https://wallabag.domain.tld" \\
    -v /var/wallabag/var:/wallabag/var \\
    -v /var/wallabag/data:/wallabag/data \\
    -v /var/wallabag/assets:/wallabag/assets \\
    -v /var/wallabag/cache:/wallabag/cache \\
    wallabag/wallabag:\$VERSION

EOF

chmod a+x /root/docker/wallabag/wallabag.sh

```

## Run Wallabag

Simply execute ```/root/docker/wallabag/wallabag.sh``` to update/run Wallabag.

## Post Install

Run the below command just after container creation to finalize installation

It creates ```wallabag/wallabag``` user/pass combo as the admin

``` bash
docker exec -it wallabag /bin/sh -c "mkdir -p /wallabag/data/db && touch /wallabag/data/db/wallabag.sqlite && php bin/console wallabag:install --no-interaction && chown -R nobody /wallabag/data /wallabag/cache /wallabag/assets /wallabag/var"

```

## Serving Via Caddy

``` bash

cat > /etc/caddy/services/wallabag.conf <<EOF
# Wallabag proxy
wallabag:80, wallabag:443, wallabag.domain.tld:80, wallabag.domain.tld:443 {
    redir 301 {
        if {scheme} is http
        /  https://wallabag.domain.tld{uri}
    }

    log /var/log/caddy/wallabag.log
    proxy / 172.30.9.9:80 {
        transparent
    }

    # Use acme.sh Let's Encrypt SSL cert setup
    tls /var/acme.sh/domain.tld/domain.tld.cer /var/acme.sh/domain.tld/domain.tld.key
}
EOF

```

## Update Unbound

``` bash

cat > /etc/unbound/local_zone/wallabag.conf <<EOF
local-data: "wallabag-insecure A 172.30.9.9"
local-data-ptr: "172.30.9.9 wallabag-insecure"
local-data: "wallabag-insecure.domain.tld A 172.30.9.9"
local-data-ptr: "172.30.9.9 wallabag-insecure.domain.tld"

local-data: "wallabag A 172.30.0.1"
local-data-ptr: "172.30.0.1 wallabag"
local-data: "wallabag.domain.tld A 172.30.0.1"
local-data-ptr: "172.30.0.1 wallabag.domain.tld"
EOF

```

# vpn.ac

```TODO : INCOMPLETE / NOT VERIFIED```

This is a **VERY** advanced topic with some creative tricks to simplify getting the config added to NetworkManager. You're on your own.

The author *strongly* recommends reading through this and adapting to other services well ahead of any real need(s).

## Inspiration / Sources

This is an adaptation of the [PIA](pia.md) setup but for vpn.ac instead.

- [https://vpn.ac/ovpn](https://vpn.ac/ovpn)

## Pep Work

Download OpenVPN configs, drop them in a good place on the filesystem and script import.

### Download OpenVPN Template Files

``` bash

mkdir -p /etc/vpn.ac
cd /etc/vpn.ac
mkdir openvpn-strong
cd openvpn-strong
wget https://vpn.ac/ovpn/AES-256-UDP.zip
unzip AES-256-UDP.zip
cd ..
wget https://vpn.ac/ovpn/ca.crt

```

### Add OpenVPN To Network Manager

``` bash

# NetworkManager Gnome is *required* to get a missing library on xenial
# OMIT Gnome item if you can get away with it...

apt update
apt install network-manager-openvpn network-manager-openvpn-gnome

```

## Import / Setup

### Manual Import

Import the cleaned up, provided OpenVPN profiles. This will require intervention on your part after the profile is created in NetworkManager to ensure it can connect properly.

#### Initial Import / Edit

``` bash

nmcli connection import type openvpn file /etc/vpn.ac/openvpn-strong/AES-256-UDP/[con_name].ovpn
nmcli con show
vim /etc/NetworkManager/system-connections/[con_name]

```
#### Adjustments/Necessary Verification

``` bash

id=vpn.ac - [con_name]

[vpn]
password-flags=0
user-name=[your_username]

[vpn-secrets]
password=[your_password]

```

#### Update Profiles in NetworkManager

``` bash

# Reload For Changes
nmcli connection reload [uuid_from_above]

# Connect
nmcli con up [uuid_from_above]

```

## Suggested Files For import

```TODO : Verify this is a reasonable subset```

- australia_sydney-aes256-udp.ovpn
- canada_toronto-aes256-udp.ovpn
- canada_vancouver-aes256-udp.ovpn
- finland-aes256-udp.ovpn
- france-aes256-udp.ovpn
- germany-aes256-udp.ovpn
- hong-kong_tseung-kwan-o-aes256-udp.ovpn
- luxembourg-aes256-udp.ovpn
- mexico-aes256-udp.ovpn
- netherlands_amsterdam-1-aes256-udp.ovpn
- norway-aes256-udp.ovpn
- singapore-aes256-udp.ovpn
- spain-aes256-udp.ovpn
- sweden-aes256-udp.ovpn
- switzerland-aes256-udp.ovpn
- uk_london-aes256-udp.ovpn
- us-central_chicago-aes256-udp.ovpn
- us-east_new-york-aes256-udp.ovpn
- us-west_seattle-aes256-udp.ovpn

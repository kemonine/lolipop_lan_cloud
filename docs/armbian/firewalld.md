# FirewallD

AKA : firewall and routing. Let's make this a firewall/router!

## Inspiration / Further Reading

- [http://www.firewalld.org/](http://www.firewalld.org/)
- [http://www.firewalld.org/documentation/howto/add-a-service.html](http://www.firewalld.org/documentation/howto/add-a-service.html)
- [http://www.firewalld.org/documentation/howto/reload-firewalld.html](http://www.firewalld.org/documentation/howto/reload-firewalld.html)
- [https://www.certdepot.net/rhel7-get-started-firewalld/](https://www.certdepot.net/rhel7-get-started-firewalld/)
- [https://fedoramagazine.org/build-network-router-firewall-fedora-22-systemd-networkd/](https://fedoramagazine.org/build-network-router-firewall-fedora-22-systemd-networkd/)
- [https://www.centos.org/forums/viewtopic.php?f=50&t=53819#p227743](https://www.centos.org/forums/viewtopic.php?f=50&t=53819#p227743)

## Configure sysctl for routing purposes

*Note: This was borrowed from the standard OpenWRT ```sysctl.conf```*

``` bash

# Setup NAT/Forwarding/Routing sysctl config
cat > /etc/sysctl.d/20-routing.conf <<EOF
net.ipv4.conf.default.arp_ignore=1
net.ipv4.conf.all.arp_ignore=1
net.ipv4.ip_forward=1
net.ipv4.icmp_echo_ignore_broadcasts=1
net.ipv4.icmp_ignore_bogus_error_responses=1
net.ipv4.igmp_max_memberships=100
net.ipv4.tcp_fin_timeout=30
net.ipv4.tcp_keepalive_time=120
net.ipv4.tcp_syncookies=1
net.ipv4.tcp_timestamps=1
net.ipv4.tcp_sack=1
net.ipv4.tcp_dsack=1

net.ipv6.conf.default.forwarding=1
net.ipv6.conf.all.forwarding=1
EOF

# Apply configuration
sysctl -p

```

## Setup FirewallD

``` bash

apt update
apt install firewalld
systemctl enable firewalld
systemctl start firewalld

```

## Verify Fundamentals

Run ```firewall-cmd --get-default-zone``` and make sure it returns ```public```.

If not run:

``` bash

firewall-cmd --set-default-zone=public
firewall-cmd --runtime-to-permanent
firewall-cmd --reload

```

## Allow internal access to ssh

``` bash

# Remove ssh from public zone
firewall-cmd --permanent --zone=public --remove-service=ssh
# Add ssh to internal zone
firewall-cmd --permanent --zone=internal --add-service ssh
# Reload rules
firewall-cmd --reload
# Verify rules
firewall-cmd --zone=public --list-all
firewall-cmd --zone=internal --list-all

```


## Useful Commands

- ```firewall-cmd --state```
- ```firewall-cmd --runtime-to-permanent```
- ```firewall-cmd --reload```
- ```firewall-cmd --get-default-zone```
- ```firewall-cmd --get-active-zones```
- ```firewall-cmd --get-zones```
- ```firewall-cmd --info-zone=[aZone]```
- ```firewall-cmd --permanent --zone=[aZone] --list-all```
- ```firewall-cmd --get-zone-of-interface=[iface]```
- ```firewall-cmd --get-log-denied```
- ```firewall-cmd --set-log-denied all```

## Tweak NetworkManager Connection Zones

``` bash

# Adjust interfaces from NetworkManager setup
firewall-cmd --permanent --zone=internal --change-interface=[wifi adapter for hot spots]
firewall-cmd --reload
nmcli connection modify wifi-ap-24 connection.zone internal
nmcli connection modify wifi-ap-50 connection.zone internal
nmcli connection modify mgmt connection.zone internal

# Verify zone configs
firewall-cmd --get-active-zones

```

## Turn on Routing

``` bash

firewall-cmd --permanent --zone=public --add-masquerade # Add NAT stuff for ipv4
firewall-cmd --permanent --query-masquerade # Make sure it's actually on
firewall-cmd --permanent --zone=internal --add-source=172.16.16.0/24
firewall-cmd --permanent --zone=internal --add-source=172.17.17.0/24
firewall-cmd --permanent --zone=internal --add-source=172.18.18.0/24
firewall-cmd --permanent --zone=internal --add-service dns --add-service dhcp
firewall-cmd --reload

```

## Other Useful Commands

Odds and ends for setting up services and the like. You can probably ignore this section. Breadcrumbs for the author really.

``` bash

firewall-cmd --permanent --zone=trusted --add-source=192.168.2.0/24
firewall-cmd --zone=internal --add-service=ssh --permanent
firewall-cmd --zone=internal --add-service={ssh,http,https,dns}
firewall-cmd --permanent --zone=public --add-port=80/tcp
firewall-cmd --permanent --zone=public --add-forward-port=port=22:proto=tcp:toport=3753:toaddr=10.0.0.1

```

# First Boot

Basic setup and configuration that's necessary to get under way.

## Adjust root password + Create First User

When you login to Armbian as root the first time you'll be prompted to set a root password and create the first user.

Follow the prompts and create an administrative user (armbian/ubuntu/admin/etc as the name) that will be used as your primary 'admin' login.

The rest of this guide assumes you'll be logged in as the admin user and will be using sudo to run all commands.

## Mirror Docs

Just in case you need reference material while offline or on a bad network link, mirror these docs to the root filesystem.

```git clone https://gitlab.com/kemonine/lolipop_lan_cloud.git /root/lolipop_lan_cloud```

## Disable root login

This will **EXPIRE** the root password. If you run this you can **NOT** login as root!!! Use with care if you're not used to sudo and/or want to retain root login for some reason.

``` bash

passwd -l root

```

## Cleanup fstab

Edit ```/etc/fstab``` and remove the ```commit=600``` block from the root filesystem definition.

## Reboot

This is necessary for the armbian setup to finish resizing the filesystem stored on the sd card.

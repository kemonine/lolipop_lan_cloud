# Network Manager

```TODO : INCOMPLETE```

Setup overall networking. This is focused on ethernet/WiFi as an internet connection.

## Inspiration / Further Reading

- [https://developer.gnome.org/NetworkManager/stable/NetworkManager.html](https://developer.gnome.org/NetworkManager/stable/NetworkManager.html)
- [https://developer.gnome.org/NetworkManager/stable/NetworkManager.conf.html](https://developer.gnome.org/NetworkManager/stable/NetworkManager.conf.html)
- [https://developer.gnome.org/NetworkManager/stable/nmcli.html](https://developer.gnome.org/NetworkManager/stable/nmcli.html)
- [https://developer.gnome.org/NetworkManager/stable/nmcli-examples.html](https://developer.gnome.org/NetworkManager/stable/nmcli-examples.html)

## Overview

Setup the base NetworkManager config/networking. This will help with making the Internet side of networking more dynamic and responsive to devices being added/removed.

The author assumes LAN/Ethernet > WiFi > 3G/LTE for connection priority. (*Note: NetworkManager assumes this too*)

## Install / Enable

``` bash

apt update
# Install additional deps
apt install ebtables ipset
# Install + add-ons
apt install network-manager \
    network-manager-openvpn network-manager-pptp
systemctl enable NetworkManager # Enable the service
systemctl start NetworkManager # Start the service

```

## Disable Stock Networking

Edit ```/etc/network/interfaces``` and make sure eth0 directives aren't present.

Reboot after above cleanup of interfaces file.

## ProTip

```nmtui``` can be used for an ncurses graphical interface for NetworkManager

## Set Hostname

``` bash

nmcli general hostname [hostname] # Additional parm sets hostname
systemctl reboot # Reboot to pickup the change

```

## Get Status

Some commands that help getting the status of NetworkManager

- ```nmcli networking connectivity```
- ```nmcli monitor```
- ```nmcli device monitor```
- ```nmcli connection monitor```

## Enable / Disable ALL

Handy if you want to shut down *all* networking for some reason

```nmcli networking on|off```

## Radio Control

Control WiFi / GSM radios

### Wifi

```nmcli radio wifi [on|off]```

### 3G/LTE

```nmcli radio wwan [on|off]```

## Connection / Device Related

Some useful commands for adjusting connection/device status

- ```nmcli connection reload # Reload any changes / updates (this isn't automagic by default)```
- ```nmcli connection show --active```
- ```nmcli connection up [id]```
- ```nmcli connection down [id]```
- ```nmcli device status```
- ```nmcli device show [ifname]```
- ```nmcli device connect [ifname]```
- ```nmcli device disconnect [ifname]```

## Disable Orange Pi Zero Internal WiFi

If you're using an Orange Pi Zero, the internal WiFi adapter is unstable at best. The following will disable the adapter.

``` bash

nmcli device status # Verify the internal WiFi is shwoing as wlan0
nmcli device disconnect wlan0 # Run this if it shows as connected
nmcli device set wlan0 autoconnect no

```

## Setup Networks

Some configuration via ```nmcli``` for various networks/interfaces/devices that may or may not be in use at any given moment. These commands just make NetworkManager aware of the overall topology and connections. Routing, firewall and more is setup later.

*Note: Add autoconnect false if you don't want the connection auto started if a device is present*

### Clear Existing

Run ```nmcli connection show``` to get a list of active network connections. We will want to remove all of these.

Run ```nmcli connection del [UUID]``` for each UUID listed in the previous commands output.

### Management Ethernet

*Note: It's assumed the on-board ethernet adapter will be used for management and an EXTERNAL USB Ethernet adapter used for WAN (if needed)*

``` bash

# Management via usb ethernet adapter
#     includes network sharing
nmcli connection add save yes \
    type ethernet \
    con-name mgmt \
    ifname eth0 \
    -- \
    ipv4.method shared \
    ipv4.addr 172.16.16.16/24 \
    ipv6.method ignore
nmcli device set eth0 autoconnect yes

```

### WiFi 2.4ghz Access Point

*Note: You can use ```802-11-wireless.channel #``` in the below command to force a channel to be used*

``` bash

# Get the ifname of the wifi adapter with `nmcli dev show`

# HostAP mode (2.4ghz / wireless access point)
#     includes network sharing
nmcli connection add save yes \
    type wifi \
    con-name wifi-ap-24 \
    ifname [wifi iface] \
    ssid 24.lolipop.domain.tld \
    -- \
    ipv4.method shared \
    ipv4.addresses 172.17.17.17/24 \
    ipv6.method ignore \
    802-11-wireless.mode ap \
    802-11-wireless.band bg \
    802-11-wireless.channel 11 \
    802-11-wireless-security.key-mgmt wpa-psk \
    802-11-wireless-security.proto rsn \
    802-11-wireless-security.psk MyPassword

```

### WiFi 5ghz Access Point

*Note: You can use ```802-11-wireless.channel #``` in the below command to force a channel to be used*

``` bash

# Get the ifname of the wifi adapter with `nmcli dev show`

# HostAP mode (5ghz / wireless access point)
#     includes network sharing
nmcli connection add save yes \
    type wifi \
    con-name wifi-ap-50 \
    ifname [wifi iface] \
    ssid 50.lolipop.domain.tld \
    -- \
    ipv4.method shared \
    ipv4.addresses 172.18.18.18/24 \
    ipv6.method ignore \
    802-11-wireless.mode ap \
    802-11-wireless.band a \
    802-11-wireless.channel 40 \
    802-11-wireless-security.key-mgmt wpa-psk \
    802-11-wireless-security.proto rsn \
    802-11-wireless-security.psk MyPassword

```

### WAN - Ethernet (External USB Adapter)

*Note: It's assumed you'll be using a USB Ethernet adapter for WAN if needed. This matches the overall use of USB devices that are plugged/unplugged as necessary for WAN needs*

``` bash

# WAN via ethernet cable
nmcli connection add save yes \
    type ethernet \
    con-name wan-eth \
    ifname eth1 \
    -- \
    ipv4.method auto \
    ipv6.method auto
nmcli device set eth1 autoconnect yes

```

### WAN - WiFi Bridge

``` bash

# Get list of access points in the area
nmcli dev wifi list

# Get the ifname of the client wifi adapter with `nmcli dev show`

# WAN via Client mode (wireless bridge)
#    Note the ASK flag so you're prompted to enter user/pass type infos
nmcli connection add save yes \
    type wifi \
    con-name wan-wifi \
    ifname [wifi iface] \
    ssid [ssidFromAbove] \
    -- \
    wifi-sec.key-mgmt wpa-psk \
    wifi-sec.psk [wpaPassword]
nmcli device set [wifi iface] autoconnect yes

```

### WAN - GSM (3G/LTE)

See [Modem Manager](modem_manager.md) for details on integrating a 3G/LTE modem into the networking setup.

## Auto Config

Once the above is setup Network Manager should handle the auto configuration of your WAN/LAN/Modems/etc for you.

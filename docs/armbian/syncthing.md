# Syncthing

A very simple way to setup/run Syncthing in a container. This approach will also update to the latest syncthing releases if available.

## Inspiration / Sources

- [https://github.com/syncthing/syncthing/releases](https://github.com/syncthing/syncthing/releases)
- [https://docs.syncthing.net/users/autostart.html#linux](https://docs.syncthing.net/users/autostart.html#linux)

## Dependencies

We need one utility to ensure we can find the latest releases. Install it.

``` bash

apt update
apt install jq

```

## Preflight Configuration

Setup basic config / storage areas ahead of install

```bash

mkdir -p /var/syncthing/.config/syncthing
groupadd syncthing
useradd -s /usr/sbin/nologin -d /var/syncthing -g syncthing syncthing
cat > /var/syncthing/.config/syncthing/config.xml <<EOF
<configuration version="28">
    <options>
        <globalAnnounceEnabled>false</globalAnnounceEnabled>
        <localAnnounceEnabled>true</localAnnounceEnabled>
        <relaysEnabled>false</relaysEnabled>
        <natEnabled>false</natEnabled>
        <minHomeDiskFree unit="%">10</minHomeDiskFree>
        <defaultFolderPath>/tank/syncthing</defaultFolderPath>
    </options>
</configuration>
EOF
chown syncthing -R /var/syncthing
chgrp syncthing -R /var/syncthing

```

## Install Syncthing

Grab the latest release of syncthing, drop it in place, setup system service.

``` bash

RELEASE=`curl -s https://api.github.com/repos/syncthing/syncthing/releases/latest | jq -r .tag_name`
ARCH=`arch`
if [ $ARCH == "aarch64" ]
then
    ARCH="arm64"
else
    ARCH="arm"
fi

gpg --keyserver keyserver.ubuntu.com --recv-key D26E6ED000654A3E
mkdir -p /tmp/syncthing
cd /tmp/syncthing
curl -sLO https://github.com/syncthing/syncthing/releases/download/${RELEASE}/syncthing-linux-${ARCH}-${RELEASE}.tar.gz
curl -sLO https://github.com/syncthing/syncthing/releases/download/${RELEASE}/sha256sum.txt.asc
gpg --verify sha256sum.txt.asc
grep syncthing-linux-${ARCH} sha256sum.txt.asc | sha256sum
tar -zxf syncthing-linux-${ARCH}-${RELEASE}.tar.gz
mv syncthing-linux-${ARCH}-${RELEASE}/syncthing /usr/bin/syncthing
chmod a+x /usr/bin/syncthing
mv syncthing-linux-${ARCH}-${RELEASE}/etc/linux-systemd/system/syncthing@.service /etc/systemd/system
systemctl daemon-reload
cd ~
rm -rf /tmp/syncthing

```

## Adjust firewall to allow syncthing on internal network(s)

``` bash

firewall-cmd --permanent --zone=internal --add-port 22000/tcp --add-port 21027/udp
# Allow GUI from docker containers (it'll be proxied by the main web proxy container for ssl purposes)
firewall-cmd --permanent --zone=trusted --add-port 22000/tcp --add-port 21027/udp --add-port 8384/tcp
firewall-cmd --reload

```

## Run Syncthing Via systemd Service

``` bash

systemctl enable syncthing@syncthing.service
systemctl start syncthing@syncthing.service

```

## Setup Update Script

Syncthing has an auto update mechanism. Script it so it can be run at any point to get updates.

``` bash

cat > /root/update_syncthing.sh <<EOF
/usr/bin/syncthing -upgrade-check
/usr/bin/syncthing -upgrade
EOF

chmod a+x /root/update_syncthing.sh

```

## Update Unbound

``` bash

cat > /etc/unbound/local_zone/syncthing.conf <<EOF
local-data: "syncthing A 172.30.0.1"
local-data-ptr: "172.30.0.1 synching"
local-data: "syncthing.domain.tld A 172.30.0.1"
local-data-ptr: "172.30.0.1 synching.domain.tld"

local-data: "syncthing-gui A 172.30.0.1"
local-data: "syncthing-gui.domain.tld A 172.30.0.1"
local-data-ptr: "172.30.0.1 syncthing-gui"
local-data-ptr: "172.30.0.1 syncthing-gui.domain.tld"
EOF

```

## Serving Via Caddy

``` bash

cat > /etc/caddy/services/syncthing.conf <<EOF
# Syncthing proxy
syncthing-gui:80, syncthing-gui:443, syncthing-gui.domain.tld:80, syncthing-gui.domain.tld:443 {
    redir 301 {
        if {scheme} is http
        /  https://syncthing-gui.domain.tld{uri}
    }

    log /var/log/caddy/syncthing.log
    proxy / 127.0.0.1:8384 {
        transparent
        header_upstream Host 127.0.0.1 # Reset the transparent proxy host so requests aren't blocked by syncthing
    }

    # Use acme.sh Let's Encrypt SSL cert setup
    tls /var/acme.sh/domain.tld/fullchain.cer /var/acme.sh/domain.tld/domain.tld.key
}
EOF

```

## Admin Interface

Once the container is online you can get to the admin interface at ```http://syncthing.domain.tld:8384```.

## Finish Configuration via GUI

- ssh router with 8384 port forward
- Open the admin interface in your browser
- Configure ```/tank/syncthing/global``` as default shared folder
  - *IF* you have a ```/tank``` available
- Set ```Minimum disk space``` to ```10%```
- Disable ```Anonymous usage reporting```
- Setup a ```GUI Authentication User``` and ```GUI Authentication Password```

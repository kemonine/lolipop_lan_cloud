# Gogs

Self hosted git repos, issue tracking and more. Think GitHub/GitLab but self hosted and lean.

## Inspiration / Further Reading

- [https://discuss.gogs.io/t/how-to-backup-restore-and-migrate/991](https://discuss.gogs.io/t/how-to-backup-restore-and-migrate/991)
- [https://blog.meinside.pe.kr/Gogs-on-Raspberry-Pi/](https://blog.meinside.pe.kr/Gogs-on-Raspberry-Pi/)

## Install / Update / Run Script

Setup a generic script that'll auto update Gogs, build a container and launch it. You should only run this script at first launch and/or when you're looking for updates.

``` bash

mkdir -p /var/gogs
chown git:root /var/gogs
mkdir /root/docker/gogs
git clone https://github.com/gogits/gogs.git /root/docker/gogs/src

cat > /root/docker/gogs/gogs.sh << EOF
#!/bin/bash

cd /root/docker/gogs/src
git fetch
LATESTTAG=\`git describe --abbrev=0 --tags\`
git checkout \$LATESTTAG

ARCH=\`arch\`
DOCKERFILE="Dockerfile"

# Cleanup arch/container image here
if [ \$ARCH == "aarch64" ]
then
    echo "64bit arm"
    DOCKERFILE="Dockerfile.aarch64"
else
    echo "32bit arm"
    DOCKERFILE="Dockerfile.rpi"
fi

docker build \\
    --network docker-private \\
    --file ./\$DOCKERFILE \\
    --tag gogs/gogs:\$LATESTTAG \\
    .

# Cleanup existing container
docker stop gogs
docker rm gogs

##########
# For postgresql instead of sqlite run the following commands
#docker exec -it postgres psql -U postgres
#create role gogs nocreatedb nocreaterole login PASSWORD 'password';
#create database gogs owner=gogs encoding=UTF8;

# Setup the below env vars using the above database/username/role and ip of 172.30.12.12
##########

# Re-run/create container with latest image
# See https://gogs.io/docs/advanced/configuration_cheat_sheet.html for more options
docker run \\
    --name gogs \\
    --restart unless-stopped \\
    --net docker-private \\
    --ip 172.30.6.6 \\
    -e TZ=UTC \\
    -e DEBUG=1 \\
    -e RUN_CROND=1 \\
    -e APP_NAME=gogs \\
    -e DOMAIN=domain.tld \\
    -e ROOT_URL=https://gogs.domain.tld \\
    -e DB_TYPE=sqlite3 \\
    -v /var/gogs:/data \\
    gogs/gogs:\$LATESTTAG
EOF

chmod a+x /root/docker/gogs/gogs.sh

```

## Run Gogs

Simply execute ```/root/docker/gogs/gogs.sh``` to update/run Gogs.

## Serving Via Caddy

``` bash

cat > /etc/caddy/services/gogs.conf <<EOF
# Gogs proxy
gogs:80, gogs:443, gogs.domain.tld:80, gogs.domain.tld:443 {
    redir 301 {
        if {scheme} is http
        /  https://gogs.domain.tld{uri}
    }

    log /var/log/caddy/gogs.log
    proxy / 172.30.6.6:3000 {
        transparent
    }

    # Use acme.sh Let's Encrypt SSL cert setup
    tls /var/acme.sh/domain.tld/fullchain.cer /var/acme.sh/domain.tld/domain.tld.key
}
EOF

```

## Update Unbound

``` bash

cat > /etc/unbound/local_zone/gogs.conf <<EOF
local-data: "gogs-insecure A 172.30.6.6"
local-data-ptr: "172.30.6.6 gogs-insecure"
local-data: "gogs-insecure.domain.tld A 172.30.6.6"
local-data-ptr: "172.30.6.6 gogs-insecure.domain.tld"

local-data: "gogs A 172.30.0.1"
local-data-ptr: "172.30.0.1 gogs"
local-data: "gogs.domain.tld A 172.30.0.1"
local-data-ptr: "172.30.0.1 gogs.domain.tld"
EOF

```

## First Run / Finalize Setup

- Navigate to ```http://gogs-insecure.domain.tld:3000```
- Follow on-screen prompts for finalizing setup
  - Be sure to specify an admin user
- Login and enjoy

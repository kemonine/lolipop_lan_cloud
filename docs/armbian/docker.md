# Docker

Containerized services for easy deployment and updates.

## Inspiration / Further Reading

- [https://docs.docker.com/install/](https://docs.docker.com/install/)
- [https://docs.docker.com/install/linux/docker-ce/ubuntu/](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [https://blog.alexellis.io/get-started-with-docker-on-64-bit-arm/](https://blog.alexellis.io/get-started-with-docker-on-64-bit-arm/)

## Pre Flight Setup

``` bash

apt remove docker docker-engine docker.io
apt install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

```

### Arm (32bit / armv7)

``` bash

add-apt-repository \
   "deb [arch=armhf] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

```

### Arm (64bit)

``` bash

add-apt-repository \
   "deb [arch=arm64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"

```

## Install

``` bash

apt update
apt install docker-ce
systemctl enable docker

```

## Adjust Storage

**OPTIONAL**

If you have an external USB storage device always connected, you may want to move the contents of ```/var/lib/docker``` to somewhere on the external storage and use a symlink in place. This will help with churn on the internal micro-sd card and extend its life.

## Create Container Script Dir

For the containers detailed here, you'll want a dedicated directory for keeping the scripts/outputs.

```mkdir /root/docker```

## Configure Docker Default Bridge

Ensure the default Docker bridge doesn't conflict with existing networks.

``` bash

cat >> /etc/docker/daemon.json <<EOF
{
  "bip": "10.30.0.1/16"
}
EOF
systemctl restart docker

```

## Setup Custom Network for Services

``` bash

docker network create \
  --subnet=172.30.0.1/16 \
  docker-private

```

## Trust Docker Private LAN

``` bash

nmcli connection show # Look for uuid of new docker bridge
nmcli connection modify [uuid] connection.zone trusted
systemctl restart NetworkManager docker firewalld
firewall-cmd --info-zone trusted

```
# Borg Backups

The BETTER backup solution.

**BE MINDFUL OF RUNNING BORG. IT CAN CAUSE PROBLEMS WITH DISK IOPS AND RAM USAGE. BEST USED WHEN THE MACHINE IS KNOWN TO BE IDLE!!!**

## Inspiration / Further Reading

- [https://borgbackup.readthedocs.io/en/stable/installation.html#from-source](https://borgbackup.readthedocs.io/en/stable/installation.html#from-source)

## Install

Note this is built using sources (kinda). May take awhile on most arm boards.

``` bash

# install build dependencies
apt update
apt install python-setuptools python3-setuptools \
    python3 python3-dev python3-pip python-virtualenv \
    libssl-dev openssl \
    libacl1-dev libacl1 \
    build-essential \
    libfuse-dev fuse pkg-config
pip3 install borgbackup[fuse]

```

## Upgrades

Per the docs

> To upgrade Borg to a new version later, run the following after activating your virtual environment:

```pip install -U borgbackup[fuse]```

## Initialize Backup Repo

*Note: assumes you have a ```/tank``` on external disk*

``` bash

cd /tank/backup
borg init --encryption none . # No crypto/auth for speed (see docs for more infos)

```

## Backup Script

Setup a backup script that backs up everything (**note the excludes**) to the initialized repository.

Run ```/root/borg_backup.sh``` any time you want to take a backup.

``` bash

cat > /root/borg_backup.sh <<EOF
#!/bin/sh
REPOSITORY=/tank/backup

# Backup all of /home and /var/www except a few
# excluded directories
/usr/local/bin/borg create -v --progress --stats -C lzma,3 \\
    \$REPOSITORY::backup-\`date +%Y-%m-%d-%H%M\` \\
    / \\
    --exclude /run \\
    --exclude /snapshots \\
    --exclude /tank \\
    --exclude /scratch \\
    --exclude /swap \\
    --exclude /proc \\
    --exclude /sys \\
    --exclude /var/lib/schroot/mount \\
    --exclude /var/lib/lxcfs \\
    --exclude /var/lib/docker \\
    --exclude /mnt

# Use the 'prune' subcommand to maintain 7 daily, 4 weekly
# and 6 monthly archives.
/usr/local/bin/borg prune -v --list \$REPOSITORY \\
    --keep-daily=7 \\
    --keep-weekly=4 \\
    --keep-monthly=2

EOF

chmod a+x /root/borg_backup.sh

```

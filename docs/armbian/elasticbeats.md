https://www.elastic.co/products/beats/filebeat
https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-installation.html
https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-configuration.html

docker pull docker.elastic.co/beats/filebeat:6.2.4

docker run --name filebeat --restart unless-stopped --net docker-private --ip 172.30.11.11 -e TZ=UTC -e DEBUG=1 docker.elastic.co/beats/filebeat:6.2.4

This can't be accomplished just yet but it looks promising for the future


https://github.com/elastic/beats-docker/tree/6.2

https://discuss.elastic.co/t/how-to-install-filebeat-on-a-arm-based-sbc-eg-raspberry-pi-3/103670/2

https://github.com/elastic/beats/tree/master/dev-tools/packer

https://s3-us-west-2.amazonaws.com/beats-package-snapshots/index.html?prefix=filebeat/

https://s3-us-west-2.amazonaws.com/beats-package-snapshots/index.html?prefix=metricbeat/
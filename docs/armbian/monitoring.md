# Monitoring

Track resource utilization over time.

**BE MINDFUL OF RUNNING THIS. IT CAN CAUSE PROBLEMS WITH DISK IOPS AND RAM USAGE. BEST ONLY USED IF NEEDED OR YOU'RE TROUBLESHOOTING.**

## cadvisor

[cadvisor (link)](https://github.com/google/cadvisor) has been recommended for monitoring Docker container resource usage. Could be useful.

Untested by the Author.

## Munin

Simple, efficient, old school, well supported. Start here.

Further reading : [http://munin-monitoring.org/](http://munin-monitoring.org/)

### Install

``` bash

apt update
apt install munin munin-node \
    munin-plugins-core munin-plugins-extra \
    libcgi-fast-perl
vim /etc/munin/munin.conf
vim /etc/munin/munin-node.conf
vim /etc/munin/plugin-conf.d/*
munin-node-configure --suggest 2>&1 | less
munin-node-configure --shell 2>&1 | less
systemcl restart munin-node
systemctl enable munin-node

```

### Serving Output Via Caddy

``` bash

cat > /etc/caddy/services/munin.conf <<EOF
# Munin - Static web resources
domain.tld:80/static domain.tld:443/static {
    redir 301 {
        if {scheme} is http
        / https://domain.tld{uri}
    }

    root /etc/munin/static

    # Use acme.sh Let's Encrypt SSL cert setup
    tls /var/acme.sh/domain.tld/fullchain.cer /var/acme.sh/domain.tld/domain.tld.key
}

# Munin - main site/cgi's
domain.tld:80 domain.tld:443 {
    redir 301 {
        if {scheme} is http
        / https://domain.tld{uri}
    }

    log /var/log/caddy/test.kemonine.info.log

    # Setup CGI rendering scripts
    cgi /munin-cgi/munin-cgi-graph /usr/lib/munin/cgi/munin-cgi-graph
    cgi /munin-cgi/munin-cgi-html /usr/lib/munin/cgi/munin-cgi-html

    # Set path to generated HTML via cron/systemd processes
    root /var/cache/munin/www

    # Use acme.sh Let's Encrypt SSL cert setup
    tls /var/acme.sh/domain.tld/fullchain.cer /var/acme.sh/domain.tld/domain.tld.key
}
EOF

```

# TT-RSS

Self hosted RSS reader a la Google Reader

## Inspiration / Further Reading
- [https://hub.docker.com/r/linuxserver/tt-rss/](https://hub.docker.com/r/linuxserver/tt-rss/)

## Install / Update / Run Script

Setup a generic script that'll auto update TT-RSS, build a container and launch it. You should only run this script at first launch and/or when you're looking for updates.

``` bash

mkdir -p /var/ttrss
docker exec -it postgres psql -U postgres
create role ttrss nocreatedb nocreaterole login PASSWORD 'password';
create database ttrss owner=ttrss encoding=UTF8;

cat > /root/docker/ttrss.sh << EOF
#!/bin/bash

ARCH=\`arch\`
HUBIMAGE=""

# Cleanup arch/container image here
if [ \$ARCH == "aarch64" ]
then
    echo "64bit arm"
    HUBIMAGE="lsioarmhf/tt-rss-aarch64:latest"
else
    echo "32bit arm"
    HUBIMAGE="lsioarmhf/tt-rss:latest"
fi

# Cleanup existing container
docker stop ttrss
docker rm ttrss

# Re-run/create container with latest image
docker run \\
    --name ttrss \\
    --restart unless-stopped \\
    --net docker-private \\
    --ip 172.30.13.13 \\
    -e TZ=UTC \\
    -e DEBUG=1 \\
    -v /var/ttrss:/config \\
    \$HUBIMAGE
EOF

chmod a+x /root/docker/ttrss.sh

```

## Run TT-RSS

Simply execute ```/root/docker/ttrss.sh``` to update/run TT-RSS.

## Serving Via Caddy

``` bash

cat > /etc/caddy/services/ttrss.conf <<EOF
# TT-RSS proxy
ttrss:80, ttrss:443, ttrss.domain.tld:80, ttrss.domain.tld:443 {
    redir 301 {
        if {scheme} is http
        /  https://ttrss.domain.tld{uri}
    }

    log /var/log/caddy/ttrss.log
    proxy / 172.30.13.13:80 {
        transparent
    }

    # Use acme.sh Let's Encrypt SSL cert setup
    tls /var/acme.sh/domain.tld/fullchain.cer /var/acme.sh/domain.tld/domain.tld.key
}
EOF

```

## Update Unbound

``` bash

cat > /etc/unbound/local_zone/ttrss.conf <<EOF
local-data: "ttrss-insecure A 172.30.13.13"
local-data-ptr: "172.30.13.13 ttrss-insecure"
local-data: "ttrss-insecure.domain.tld A 172.30.13.13"
local-data-ptr: "172.30.13.13 ttrss-insecure.domain.tld"

local-data: "ttrss A 172.30.0.1"
local-data-ptr: "172.30.0.1 ttrss"
local-data: "ttrss.domain.tld A 172.30.0.1"
local-data-ptr: "172.30.0.1 ttrss.domain.tld"
EOF

```

## First Run / Finalize Setup

- Navigate to ```http://ttrss-insecure.domain.tld```
- Follow on-screen prompts for finalizing setup
  - Use the above psql username/password/database during the install phase
  - Use 172.30.12.12 as the postgresql hostname
- Login and enjoy
  - The default account is admin/password

# Incron

A 'cron' daemon that watches the filesystem for changes and performs actions. This is used to auto-apply config for Caddy and Unbound.

This is here mainly to facilitate auto-reloads of Caddy/Unbound during setup and over time. This is especially helpful if using acme.sh for Let's Encrypt certificates as they are regularly updated and once updated need a restart of the Caddy service. This config takes care of that situation as well as updated DNS records in Unbound.

## Inspiration / Sources

- [https://www.cyberciti.biz/faq/linux-inotify-examples-to-replicate-directories/](https://www.cyberciti.biz/faq/linux-inotify-examples-to-replicate-directories/)

## Install

``` bash

apt update
apt install incron
systemctl enable incron
systemctl start incron

```

## Configure

### Unbound

``` bash

cat > /etc/incron.d/unbound.conf <<EOF
/etc/unbound/local_zone IN_CREATE,IN_ATTRIB,IN_MODIFY,IN_DELETE unbound-control reload
EOF

```

### Caddy

``` bash

cat > /etc/incron.d/caddy.conf <<EOF
/etc/caddy/services IN_CREATE,IN_ATTRIB,IN_MODIFY,IN_DELETE killall -USR1 caddy
EOF

```

### Acme.sh Certificate Updates

``` bash

cat > /etc/incron.d/acme-sh.conf <<EOF
/var/acme.sh/domain.tld IN_CREATE,IN_ATTRIB,IN_MODIFY,IN_DELETE killall -USR1 caddy
EOF

```

### Post Configure

``` bash

systemctl restart incron

```

# Cockpit

This is **OPTIONAL** but can provide some helpful tools/insights while in the field.

Notably Cockpit includes a terminal via a web browser. Perfect for on-the-go tuning of network settings and/or disaster recovery.

Further reading: [http://cockpit-project.org/running](http://cockpit-project.org/running)

## Install

``` bash

#add-apt-repository ppa:cockpit-project/cockpit
apt update
apt install cockpit cockpit-doc \
    cockpit-docker cockpit-networkmanager \
    cockpit-dashboard cockpit-system \
    cockpit-storaged cockpit-packagekit
systemctl enable cockpit
systemctl start cockpit

```

## Config

Leave the defaults (*including SSL certificate*). This is here to save you on the fly or when not near a 'full' computer. Defaults are a good thing at times.

## Allow internal access

If you already setup firewalld, run the following to allow access from the *INTERNAL* network only.

``` bash

firewall-cmd --permanent --zone=internal --add-port=9090/tcp
firewall-cmd --reload

```

## Grant Admin User(s) Access to Docker

``` bash

usermod -aG docker [admin_user]
systemctl restart docker

```

# Unbound

Caching DNS that uses the roots instead of ISP/other DNS servers

## Inspiration / Further Reading

- [https://wiki.archlinux.org/index.php/Unbound](https://wiki.archlinux.org/index.php/Unbound)

## Install / Base Setup

``` bash

apt update
apt install unbound unbound-host

mkdir /etc/unbound/local_zone
curl -o /etc/unbound/root.hints https://www.internic.net/domain/named.cache
cat > /etc/unbound/root.key <<EOF
. IN DS 19036 8 2 49AAC11D7B6F6446702E54A1607371607A1A41855200FD2CE1CDDE32F24E8FB5
EOF
chown unbound /etc/unbound
cat > /etc/unbound/unbound.conf <<EOF
server:
    interface: 127.0.0.1
    port: 53
    hide-identity: yes
    hide-version: yes
    num-threads: 1
    root-hints: "/etc/unbound/root.hints"
    cache-min-ttl: 60
    logfile: /var/log/unbound
    use-syslog: yes
    do-ip4: yes
    #do-ip6: no
    do-udp: yes
    do-tcp: yes
    domain-insecure: * # Comment this out if you have a proper RTC
    verbosity: 1
    minimal-responses: yes
    prefetch: yes
    rrset-roundrobin: yes
    use-caps-for-id: yes
    harden-glue: yes
    harden-dnssec-stripped: yes
    auto-trust-anchor-file: "/etc/unbound/root.key"
    val-clean-additional: yes
    private-domain: "[domain.tld]"
    private-address: 192.168.0.0/16
    private-address: 172.16.0.0/12
    private-address: 10.0.0.0/8
    access-control: 172.30.0.0/16 allow
    access-control: 172.16.16.0/24 allow
    access-control: 172.17.17.0/24 allow
    access-control: 172.18.18.0/24 allow

include: /etc/unbound/local_zone/*.conf

EOF

systemctl enable unbound
systemctl start unbound

unbound-host -C /etc/unbound/unbound.conf -v sigok.verteiltesysteme.net
unbound-host -C /etc/unbound/unbound.conf -v sigfail.verteiltesysteme.net

cat > /etc/systemd/system/roothints.service <<EOF
[Unit]
Description=Update root hints for unbound
After=network.target

[Service]
ExecStart=/usr/bin/curl -o /etc/unbound/root.hints https://www.internic.net/domain/named.cache
EOF

cat > /etc/systemd/system/roothints.timer <<EOF
[Unit]
Description=Run root.hints monthly

[Timer]
OnCalendar=monthly
Persistent=true

[Install]
WantedBy=timers.target
EOF

systemctl daemon-reload
systemctl enable roothints.timer
systemctl start roothints.timer

```

## Adjust NetworkManager Config

Edit ```/etc/NetworkManager/NetworkManager.conf```

Change the ```dns=``` line to be ```dns=127.0.0.1```

## Setup all WAN connections to use this for dns cache

### WiFi

``` bash

nmcli con modify wan-wifi \
    ipv4.ignore-auto-dns yes \
    ipv6.ignore-auto-dns yes
nmcli con modify wan-wifi \
    ipv4.dns "127.0.0.1"

```

### Ethernet

``` bash

nmcli con modify wan-eth \
    ipv4.ignore-auto-dns yes \
    ipv6.ignore-auto-dns yes
nmcli con modify wan-eth \
    ipv4.dns "127.0.0.1"

```

### USB 3G/LTE

``` bash

nmcli con modify wan-wwan-1 \
    ipv4.ignore-auto-dns yes \
    ipv6.ignore-auto-dns yes
nmcli con modify wan-wwan-1 \
    ipv4.dns "127.0.0.1"

```

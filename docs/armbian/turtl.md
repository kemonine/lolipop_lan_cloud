# Turtl

Self hosted Evernote/OneNote. Take some notes already.

## Inspiration / Further Reading

- [https://github.com/turtl/server](https://github.com/turtl/server)
- [https://turtlapp.com/docs/server/](https://turtlapp.com/docs/server/)
- [https://turtlapp.com/](https://turtlapp.com/)

## Prep

``` bash

mkdir -p /var/turtl/plugins
mkdir -p /root/docker/turtl
git clone https://github.com/turtl/server.git /root/docker/turtl/src
docker exec -it postgres psql -U postgres
create role turtl nocreatedb nocreaterole login PASSWORD 'password';
create database turtl owner=turtl encoding=UTF8;

cat > /root/docker/turtl/src/config/config.yaml <<EOF
---
server:
  port: 8181

db:
  host: '172.30.12.12'
  port: 5432
  database: 'turtl'
  user: 'turtl'
  password: 'password'
  pool: 24

loglevel: 'debug'

app:
  # ALWAYS false in production. Always.
  # Set to 'I UNDERSTAND THIS VIOLATES THE PRIVACY OF MY USERS' to enable
  enable_bookmarker_proxy: false
  # no trailing slash
  api_url: 'http://172.30.10.10:8181'
  www_url: 'https://172.30.10.10'
  emails:
    admin: 'admin@turtlapp.com'
    info: 'Turtl <info@turtlapp.com>'
    invites: 'invites@turtlapp.com'
  # TODO: replace this with a long, unique value. seriously. write down a dream
  # you had, or the short story you came up with during your creative writing
  # class in your freshmen year of college. have fun with it.
  secure_hash_salt: "Plaque is a figment of the liberal media and the dental industry to scare you into buying useless appliances and pastes. Now, I've read the arguments on both sides and I haven't found any evidence yet to support the need to brush your teeth. Ever."

sync:
  # how many sync records can a client send at a time? it's a good idea to have
  # a limit here, lest a rogue client flood the server with sync items
  max_bulk_sync_records: 32

plugins:
  plugin_location: '/var/www/turtl/server/plugins'
  analytics:
    enabled: false
  email:
    enabled: false
  premium:
    enabled: false

uploads:
  # if set to a path, files will be uploaded to the local filesystem instead of
  # S3. otherwise, set to false
  local: '/var/www/turtl/server/public/uploads'
  # if true, downloading local files will be proxied through the turtl server.
  # this avoids needing to set up any CORS config in your favorite webserver,
  # but may slightly affect performance on high-demand servers.
  local_proxy: true
  # if local_proxy is false, this is should be the url path the uploaded files
  # are publicly available on
  url: 'http://api.turtl.dev/uploads'

s3:
  token: 'IHADAPETSNAKEBUTHEDIEDNOOOOO'
  secret: ''
  bucket: ''
endpoint: 'https://s3.amazonaws.com'
EOF

```

## Install / Setup

``` bash

cat > /root/docker/turtl/turtl.sh <<EOF
cd /root/docker/turtl/src
git checkout Dockerfile
git pull

VERSION="HEAD"

ARCH=\`arch\`

# Cleanup arch/container image here
if [ \$ARCH == "aarch64" ]
then
    echo "64bit arm"
    sed -i 's_node:8.9.4-alpine_arm64v8/node:8.9.4-alpine_g' Dockerfile
else
    echo "32bit arm"
    sed -i 's_node:8.9.4-alpine_arm32v7/node:8.9.4-alpine_g' Dockerfile
fi

sed -i 's_config/config.yaml.default_config/config.yaml_g' Dockerfile

docker build \\
    --network docker-private \\
    --file ./Dockerfile \\
    --tag turtl/turtl:\$VERSION \\
    .

# Cleanup existing container
docker stop turtl
docker rm turtl

# Re-run/create container with latest image
docker run \\
    --name turtl \\
    --restart unless-stopped \\
    --net docker-private \\
    --ip 172.30.10.10 \\
    -e TZ=UTC \\
    -e DEBUG=1 \\
    -v /var/turtl:/var/www/turtl/server \\
    turtl/turtl:\$VERSION

EOF

chmod a+x /root/docker/turtl/turtl.sh

```

## Run Turtl

Simply execute ```/root/docker/turtl/turtl.sh``` to update/run Turtl.

## Serving Via Caddy

``` bash

cat > /etc/caddy/services/turtl.conf <<EOF
# Turtl proxy
turtl:80, turtl:443, turtl.domain.tld:80, turtl.domain.tld:443 {
    redir 301 {
        if {scheme} is http
        /  https://turtl.domain.tld{uri}
    }

    log /var/log/caddy/turtl.log
    proxy / 172.30.9.9:80 {
        transparent
    }

    # Use acme.sh Let's Encrypt SSL cert setup
    tls /var/acme.sh/domain.tld/fullchain.cer /var/acme.sh/domain.tld/domain.tld.key
}
EOF

```

## Update Unbound

``` bash

cat > /etc/unbound/local_zone/turtl.conf <<EOF
local-data: "turtl-insecure A 172.30.10.10"
local-data-ptr: "172.30.10.10 turtl-insecure"
local-data: "turtl-insecure.domain.tld A 172.30.10.10"
local-data-ptr: "172.30.10.10 turtl-insecure.domain.tld"

local-data: "turtl A 172.30.0.1"
local-data-ptr: "172.30.0.1 turtl"
local-data: "turtl.domain.tld A 172.30.0.1"
local-data-ptr: "172.30.0.1 turtl.domain.tld"
EOF

```

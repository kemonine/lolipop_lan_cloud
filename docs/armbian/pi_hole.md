# Pi Hole

Ad blocking at the DNS level. Save yourself that precious transfer while on the go.

This was chosen as it's designed to run on a Raspberry Pi and... this project is all about that kind of hardware.

*NOTE: You may want to use a filesystem on a USB disk instead of /var for the volumes setup in the below Docker command(s) to help reduce writes to the micro sd card*

## Inspiration / Further Reading

- [https://hub.docker.com/r/diginc/pi-hole/](https://hub.docker.com/r/diginc/pi-hole/)
- [https://hub.docker.com/r/diginc/pi-hole-multiarch/tags/](https://hub.docker.com/r/diginc/pi-hole-multiarch/tags/)
- [https://github.com/diginc/docker-pi-hole/blob/master/README.md](https://github.com/diginc/docker-pi-hole/blob/master/README.md)
- [https://www.bentasker.co.uk/documentation/linux/279-unbound-adding-custom-dns-records](https://www.bentasker.co.uk/documentation/linux/279-unbound-adding-custom-dns-records)

## Update Unbound

### Setup unbound to listen on the Docker LAN so it can be the upstream of Pi Hole

Add a 2nd ```interface``` line to ```/etc/unbound/unbound.conf```

```interface: 172.30.0.1```

Restart unbound with ```systemctl restart unbound```

## Setup Unbound to start *after* Docker

*See [here (link)](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-Managing_Services_with_systemd-Unit_Files#brid-Managing_Services_with_systemd-Extending_Unit_Config) for more details.*

This is mainly here to ensure that unbound starts *after* the Docker network comes up as it's configured to listen on the Docker network. It'll fail to load/restart if the bind address isn't present when it is started.

``` bash

mkdir -p /etc/systemd/system/unbound.service.d
cat > /etc/systemd/system/unbound.service.d/10-after-docker.conf <<EOF
[Unit]
Requires=docker.service
After=docker.service
Restart=always
RestartSec=10
EOF
systemctl daemon-reload

```

## Setup Initial Run & Update Script

A simple update script that will pull the latest Pi Hole Docker image, configure it for auto-run, etc. Note the settings under the ```docker run``` command. You need/want to tweak them lightly.

Full docs on run time parms can be found in the Pi Hole [docs (link)](https://github.com/diginc/docker-pi-hole/blob/master/README.md).

``` bash

mkdir /var/pihole /var/pihole/data /var/pihole/dnsmasq.d
cat > /root/docker/pi-hole.sh <<EOF
#!/bin/bash

ARCH=\`arch\`
UPSTREAM=""

# Cleanup arch/container image here
if [ \$ARCH == "aarch64" ]
then
    echo "64bit arm"
    UPSTREAM="diginc/pi-hole-multiarch:debian_aarch64"
else
    echo "32bit arm"
    UPSTREAM="diginc/pi-hole-multiarch:debian_armhf"
fi

echo "Updating"

docker pull \$UPSTREAM

# Cleanup existing container
docker stop pi-hole
docker rm pi-hole

# Re-run/create container with latest image
docker run \\
    --name pi-hole \\
    --restart unless-stopped \\
    --memory=128m \\
    --net docker-private \\
    --ip 172.30.5.5 \\
    -e ServerIP=172.30.5.5 \\
    -e DNS1=172.30.0.1 \\
    -e TZ=UTC \\
    -e WEBPASSWORD=[adecentpassword] \\
    -e DEBUG=1 \\
    -v /var/pihole/data:/etc/pihole \\
    -v /var/pihole/dnsmasq.d:/etc/dnsmasq.d \\
    \$UPSTREAM

EOF

chmod a+x /root/docker/pi-hole.sh

```

## Run Pi Hole

Simply execute ```/root/docker/pi-hole.sh``` to update/run Pi Hole.

## Update LAN(s) to Use Pi Hole

*Note: Do NOT update the WAN connections to use Pi Hole. The only 'thing' using the WAN dns (unbound) should be the main board which should not be affected by ads. This also simplifies troubleshooting and failure modes (the board won't need working Docker/Pi Hole to fix problems with Docker/Pi Hole).*

``` bash

# Ensure *ALL* shared connections use pi hole (creative trick with NetworkManager)
cat > /etc/NetworkManager/dnsmasq-shared.d/pi-hole.conf <<EOF
server=172.30.5.5
EOF

# Bounce LAN's to pickup changes
nmcli con down mgmt && nmcli con up mgmt
nmcli con down wifi-ap-50 && nmcli con up wifi-ap-50
nmcli con down wifi-ap-24 && nmcli con up wifi-ap-24

```

## Update Unbound

``` bash

cat > /etc/unbound/local_zone/pi-hole.conf <<EOF
local-data: "pi-hole A 172.30.5.5"
local-data-ptr: "172.30.5.5 pi-hole"
local-data: "pi-hole.domain.tld A 172.30.5.5"
local-data-ptr: "172.30.5.5 pi-hole.domain.tld"

local-data-ptr: "172.30.0.1 pi-hole-gui"
local-data-ptr: "172.30.0.1 pi-hole-gui.domain.tld"
local-data: "pi-hole-gui A 172.30.0.1"
local-data: "pi-hole-gui.domain.tld A 172.30.0.1"
EOF

```

## Serving Via Caddy

``` bash

cat > /etc/caddy/services/pi-hole.conf <<EOF
# Pi Hole proxy
#    This is only so good
#    Pi Hole assumes everything is http. Bump it over to http instead of https, because Pi Hole is stupid at life
pi-hole-gui:80, pi-hole-gui:443, pi-hole-gui.domain.tld:80 pi-hole-gui.domain.tld:443 {
    redir 301 {
        if {scheme} is https
        /  http://pi-hole-gui.domain.tld{uri}
    }

    log /var/log/caddy/pi-hole.log
    proxy / 172.30.5.5:80 {
        transparent
    }

    # Use acme.sh Let's Encrypt SSL cert setup
    tls /var/acme.sh/domain.tld/fullchain.cer /var/acme.sh/domain.tld/domain.tld.key
}
EOF

```

## Admin Interface

Once the container is online you can get to the Pi Hole admin interface at ```http://pi-hole.domain.tld/admin```.

## First Run Config

Upon first run Pi Hole will be using a Google DNS server as a secondary to the locally hosted Unbound instance. You'll likely want to disable this functionality.

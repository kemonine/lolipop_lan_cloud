# Modem Manager

Setup an LTE/3G modem. You'll need to adapt this information to your specific modem(s) but overall the process is sound.

## Inspiration / Further Reading

- [https://www.freedesktop.org/software/ModemManager/man/1.0.0/mmcli.8.html](https://www.freedesktop.org/software/ModemManager/man/1.0.0/mmcli.8.html)
- [https://superuser.com/questions/1228477/linux-gsm-nmcli-working-with-two-gsm-modems-and-connections](https://superuser.com/questions/1228477/linux-gsm-nmcli-working-with-two-gsm-modems-and-connections)
- [https://unix.stackexchange.com/questions/113975/configure-gsm-connection-using-nmcli](https://unix.stackexchange.com/questions/113975/configure-gsm-connection-using-nmcli)
- [http://www.draisberghof.de/usb_modeswitch/bb/viewtopic.php?t=836](http://www.draisberghof.de/usb_modeswitch/bb/viewtopic.php?t=836)
- [https://wiki.openwrt.org/doc/recipes/3gdongle](https://wiki.openwrt.org/doc/recipes/3gdongle)
- [http://www.draisberghof.de/usb_modeswitch/#download](http://www.draisberghof.de/usb_modeswitch/#download)
- [http://www.draisberghof.de/usb_modeswitch/#install](http://www.draisberghof.de/usb_modeswitch/#install)
- [http://www.draisberghof.de/usb_modeswitch/#install](http://www.draisberghof.de/usb_modeswitch/#install)
- [https://forums.linuxmint.com/viewtopic.php?f=53&t=119342](https://forums.linuxmint.com/viewtopic.php?f=53&t=119342)
- [https://bostonenginerd.com/posts/getting-the-t-mobile-jet-huawei-366-usb-modem-to-work-in-linux/](https://bostonenginerd.com/posts/getting-the-t-mobile-jet-huawei-366-usb-modem-to-work-in-linux/)
- [https://github.com/Robpol86/robpol86.com/blob/master/docs/raspberry_pi_project_fi.rst](https://github.com/Robpol86/robpol86.com/blob/master/docs/raspberry_pi_project_fi.rst)

## On The Go Help

If you're ever in need of help on the go, ```man mmcli``` has a lot of good info as does ```mmcli --help```.

## Initial Setup

The steps below are for getting USB mode switch working which is necessary for some Huawei modems. This example is for a Huawei 366 modem. You'll need to adjust your setup accordingly.

``` bash

apt update
apt install usb-modeswitch usb-modeswitch-data
cd /etc/usb_modeswitch.d
tar -xzf /usr/share/usb_modeswitch/configPack.tar.gz
lsusb # note modem ids
# Huawei e366
usb_modeswitch --default-vendor 12d1 --default-product 1446 -c /etc/usb_modeswitch.d/12d1\:1446
lsusb # verify the modem ids changed (may take a moment to mode switch)

```

## Modem Manager Setup / Preflight

``` bash

apt update
apt install modemmanager # Install
systemctl enable ModemManager # Enable the service
systemctl start ModemManager # Start the service
mmcli --scan-modems # Scan for modems (this can take a few minutes)
mmcli --list-modems # List the modems modem manager sees
mmcli --modem 0 # Get details of first modem

```

## Setup Auto Mode Switch

This should be setup by the ```usb_modeswitch``` package for you. Reboot and then run ```lspci``` to ensure it's 100%.

## Useful mmcli Commands / Switches

- ```mmcli --monitor-modems```
- ```mmcli --enable```
- ```mmcli --disable```
- ```mmcli --monitor-state```
- ```--set-power-state-on```
- ```--set-power-state-low```
- ```--set-power-state-off```

## Add connection to NetworkManager

``` bash

# Ensure things work
mmcli --modem 0 --enable
mmcli --modem 0 --list-bearers
mmcli --modem 0 --3gpp-scan --timeout=30

# Simple connection (this does no good in production)
mmcli --modem 0 --simple-connect="pin=1234,apn=internet"

# Setup persistent connection via NetworkManager
nmcli c add con-name "wan-wwan-1" type gsm ifname "*" apn "internet"

```

## Location Related Commands

``` bash

mmcli --modem 0 --location-status
mmcli --modem 0 --location-enable-3gpp
mmcli --modem 0 --location-enable-gps-nmea
mmcli --modem 0 --location-enable-gps-raw
mmcli --modem 0 --location-get

```

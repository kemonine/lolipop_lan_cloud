# Postgresql

A database for all those awesome services you'll be running.

## Install / Update / Run Script

Setup a generic script that'll auto update , build a container and launch it. You should only run this script at first launch and/or when you're looking for updates.

``` bash

mkdir -p /var/postgres/data

cat > /root/docker/postgres.sh << EOF
#!/bin/bash

VERSION=""

ARCH=\`arch\`

# Cleanup arch/container image here
if [ \$ARCH == "aarch64" ]
then
    echo "64bit arm"
    VERSION="arm64v8/postgres:latest"
else
    echo "32bit arm"
    VERSION="arm32v7/postgres:latest"
fi

docker pull \$VERSION

# Cleanup existing container
docker stop postgres
docker rm postgres

# Re-run/create container with latest image
docker run \\
    --name postgres \\
    --restart unless-stopped \\
    --net docker-private \\
    --ip 172.30.12.12 \\
    -e TZ=UTC \\
    -e DEBUG=1 \\
    -e POSTGRES_PASSWORD=test1234 \\
    -v /var/postgres/data:/var/lib/postgresql/data \\
    \$VERSION
EOF

chmod a+x /root/docker/postgres.sh

```

## Run Postgres

Simply execute ```/root/docker/postgres.sh``` to update/run Postgres.

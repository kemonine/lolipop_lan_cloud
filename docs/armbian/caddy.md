# Web Service Proxy (caddy)

Simple and efficient go based proxy server and static web host. TLS and more supported out of the box. Supports all kinds of arch's and you probably want to just use this as it's the simplest approach and leanest.

## SSL Certs

This assumes you've run the [Let's Encrypt](lets_encrypt.md) process to get your certificates setup properly.

## Inspiration / Sources

- [https://caddyserver.com/](https://caddyserver.com/)
- [https://github.com/lucaslorentz/caddy-docker-proxy](https://github.com/lucaslorentz/caddy-docker-proxy)

## Docker Integration

Please note the Docker plugin is for a *swarm* which is *not* setup in these docs. It does **NOT** apply to this build.

## Install

``` bash

mkdir /var/log/caddy
mkdir -p /etc/caddy/services
chown www-data /var/log/caddy /etc/caddy
cat > /root/update_caddy.sh <<EOF
curl https://getcaddy.com | bash -s personal http.cache,http.cgi,http.cors,http.expires,http.filemanager,http.ipfilter,http.locale,http.realip,http.upload,net
EOF
chmod a+x /root/update_caddy.sh
/root/update_caddy.sh

```

## Configure

Setup a basic config for all services provided by the SBC. Pi Hole, NextCloud, Syncthing UIs all behind a SSL/TLS capable proxy.

``` bash

cat > /etc/caddy/Caddyfile <<EOF
# Individual configs are in their own files
import /etc/caddy/services/*.conf
EOF

```

## Adjust firewall to allow caddy on internal network(s)

``` bash

firewall-cmd --permanent --zone=internal --add-service http --add-service https
firewall-cmd --permanent --zone=trusted --add-service http --add-service https
firewall-cmd --reload

```

## Grant access to SSL certificates

``` bash

apt install acl
setfacl -m www-data:rx /var/acme.sh/
setfacl -m www-data:rx /var/acme.sh/domain.tld
setfacl -m www-data:r /var/acme.sh/domain.tld/fullchain.cer
setfacl -m www-data:r /var/acme.sh/domain.tld/domain.tld.cer
setfacl -m www-data:r /var/acme.sh/domain.tld/domain.tld.key
mkdir /etc/ssl/caddy
chown www-data /etc/ssl/caddy

```

## Run via systemd

``` bash

wget -O /etc/systemd/system/caddy.service https://raw.githubusercontent.com/mholt/caddy/master/dist/init/linux-systemd/caddy.service
chown root:root /etc/systemd/system/caddy.service
chmod 644 /etc/systemd/system/caddy.service
systemctl daemon-reload
systemctl enable caddy.service

```

## Update Caddy

To update Caddy, run the script that was setup during install : ```/root/update_caddy.sh```. That's it, you'll download the latest version and update in-place. Simple.

## Update unbound

*Serve the IP address of the proxy for the services with web interfaces*

``` bash

cat > /etc/unbound/local_zone/caddy.conf <<EOF
local-data: "domain.tld A 172.30.0.1"
local-data-ptr: "172.30.0.1 domain.tld"
EOF

```

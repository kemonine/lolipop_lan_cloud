# NextCloud

NextCloud in a container. A simple PHP-FPM deployment. You'll need the Web Server container setup to get access. This just gives a very basic, non-web-server version of NextCloud.

*NOTE: You may want to use a filesystem on a USB disk instead of /var for the volumes setup in the below Docker command(s) to help reduce writes to the micro sd card*

## Inspiration / Sources

- [https://github.com/nextcloud/docker](https://github.com/nextcloud/docker)
- [https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion)
- [https://hub.docker.com/_/nextcloud/](https://hub.docker.com/_/nextcloud/)
- [https://hub.docker.com/r/arm64v8/nextcloud/](https://hub.docker.com/r/arm64v8/nextcloud/)
- [https://hub.docker.com/r/arm32v5/nextcloud/](https://hub.docker.com/r/arm32v5/nextcloud/)
- [https://hub.docker.com/r/arm32v7/nextcloud/](https://hub.docker.com/r/arm32v7/nextcloud/)

## Install / Update / Run Script

Setup a generic script that'll auto update NextCloud, build a container and launch it. You should only run this script at first launch and/or when you're looking for updates.

``` bash

mkdir /var/nextcloud
chown www-data /var/nextcloud

cat > /root/docker/nextcloud.sh <<EOF
#!/bin/bash

ARCH=\`arch\`
UPSTREAM="arm32v7/nextcloud:stable"

# Cleanup arch/container image here
if [ \$ARCH == "aarch64" ]
then
    echo "64bit arm"
    UPSTREAM="arm64v8/nextcloud:stable"
else
    echo "32bit arm"
    UPSTREAM="arm32v7/nextcloud:stable"
fi

echo "Updating"

docker pull \$UPSTREAM

echo "Running with latest release"

# Cleanup existing container
docker stop nextcloud
docker rm nextcloud

##########
# For postgresql instead of sqlite run the following commands
#docker exec -it postgres psql -U postgres
#create role nextcloud nocreatedb nocreaterole login PASSWORD 'password';
#create database nextcloud owner=nextcloud encoding=UTF8;

# Setup using the above database/username/role and ip of 172.30.12.12
##########

# Re-run/create container with latest image
docker run \\
    --name nextcloud \\
    --restart unless-stopped \\
    --net docker-private \\
    --ip 172.30.7.7 \\
    -e TZ=UTC \\
    -e DEBUG=1 \\
    -v /var/nextcloud:/var/www/html \\
    \$UPSTREAM

EOF

chmod a+x /root/docker/nextcloud.sh

```

## Run NextCloud

Simply execute ```/root/docker/nextcloud.sh``` to update/run NextCloud.

## Update Unbound

``` bash

cat > /etc/unbound/local_zone/nextcloud.conf <<EOF
local-data: "nextcloud-insecure A 172.30.7.7"
local-data-ptr: "172.30.7.7 nextcloud-insecure"
local-data: "nextcloud-insecure.domain.tld A 172.30.7.7"
local-data-ptr: "172.30.7.7 nextcloud-insecure.domain.tld"

local-data: "nextcloud A 172.30.0.1"
local-data: "nextcloud.domain.tld A 172.30.0.1"
local-data-ptr: "172.30.0.1 nextcloud"
local-data-ptr: "172.30.0.1 nextcloud.domain.tld"
EOF

```

## Serving Via Caddy

``` bash

cat > /etc/caddy/services/nextcloud.conf <<EOF
# Nextcloud proxy
nextcloud:80, nextcloud:443, nextcloud.domain.tld:80, nextcloud.domain.tld:443 {
    redir 301 {
        if {scheme} is http
        /  https://nextcloud.domain.tld{uri}
    }

    log /var/log/caddy/nextcloud.log
    proxy / 172.30.7.7:80 {
        transparent
    }

    # Use acme.sh Let's Encrypt SSL cert setup
    tls /var/acme.sh/domain.tld/fullchain.cer /var/acme.sh/domain.tld/domain.tld.key

    header / {
        # Enable HTTP Strict Transport Security (HSTS) to force clients to always
        # connect via HTTPS (do not use if only testing)
        Strict-Transport-Security "max-age=15552000;"
        # Enable cross-site filter (XSS) and tell browser to block detected attacks
        X-XSS-Protection "1; mode=block"
        # Prevent some browsers from MIME-sniffing a response away from the declared Content-Type
        X-Content-Type-Options "nosniff"
        # Disallow the site to be rendered within a frame (clickjacking protection)
        X-Frame-Options "DENY"
    }
}
EOF

```

## First Run / Finalize Setup

- Navigate to ```http://nextcloud-insecure.domain.tld```
- Follow on-screen prompts for finalizing the NextCloud setup
- Login as Admin

## Post Install

### Update/Install/Enable Apps

#### Enabled Apps
- Update any apps that are showing as out of date

#### Disabled apps

- Enable Auditing / Logging app
- Enable Default encryption module
- Enable external storage support
- Enable PDF Viewer

#### Customization

- Install External sites app

#### Files

- Install Group folders app

#### Office & Text

- Enable Calendar app
- Enable Contacts app
- Enable Notes app
- Enable Tasks app

#### Organization

- Install Annoucement center app
- Enable bookmarks app

#### Security

- Enable brute force settings app
- Enable restrict login to IP addresses app
- Enable Two Factor TOTP Provider app
- Enable Two Factor U2F app
- Enable Two Factory Yubikey

#### Social & communication

- Enable circles app

#### Tools

- Enable Impersonate app

### Basic Setup

#### Adjust default view

If you'd like to see the activities view as your default view in NextCloud, edit ```/var/nextcloud/config/config.php``` and add ```'defaultapp' => 'activity',``` to the file.

#### Add Cronjob

In the settings change from ```Ajax``` for scheduled jobs to ```Cron``` and run the following commands on your device.

This will lessen the page loads and keep the cron job constrained to a reasonable duration.

``` bash

cat > /etc/systemd/system/nextcloudcron.service <<EOF
[Unit]
Description=Nextcloud cron.php job

[Service]
User=roo
ExecStart=/usr/bin/docker exec --user www-data nextcloud php /var/www/html/cron.php

[Install]
WantedBy=basic.target
EOF

cat > /etc/systemd/system/nextcloudcron.timer <<EOF
[Unit]
Description=Run Nextcloud cron.php every 90 minutes

[Timer]
OnBootSec=10min
OnUnitActiveSec=90min
Unit=nextcloudcron.service

[Install]
WantedBy=timers.target
EOF

systemctl daemon-reload
systemctl start nextcloudcron.timer
systemctl enable nextcloudcron.timer

```

#### Adjust Sharing settings

- Disable ```Allow public uploads```
- Disable ```Allow users on this server to send shares to other servers```
- Disable ```Send password by mail```

#### Adjust Security settings

Recommended Settings (Up to you)

- Minimal Length : 12
- Forbid common passwords
- Enforce upper and lower case characters
- Enforce numeric characters

### Setup Apps

- Setup file encryption : [https://docs.nextcloud.com/server/13/admin_manual/configuration_files/encryption_configuration.html](https://docs.nextcloud.com/server/13/admin_manual/configuration_files/encryption_configuration.html)
- Setup external sites app as appropriate
- Setup remaining apps from above

### Configure groups (as appropriate)

- Create group for standard users
- Create group folder for the new group (non-syncthing dumping ground for sync)
- Setup shared contacts list for new group
- Setup shared calendar for new group

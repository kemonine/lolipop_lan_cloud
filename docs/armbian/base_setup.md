# Base Setup

Fundamental setup after first boot/reboot.

## Pro Tip

```sudo -sHu root``` is a handy trick when doing a lot of administrative stuff at the command line

## Upgrade Packages

``` bash

apt update
apt upgrade
systemctl reboot

```

## DISABLE Automatic Update Downloads

*Note: this is to save bandwidth, time, etc when travelling*

### /etc/apt/apt.conf.d/02periodic

Change ```APT::Periodic::Enable "1";``` to ```APT::Periodic::Enable "0";```

### /etc/apt/apt.conf.d/20auto-upgrades

Change ```APT::Periodic::Update-Package-Lists "1";``` to ```APT::Periodic::Update-Package-Lists "0";```

Change ```APT::Periodic::Unattended-Upgrade "1";``` to ```APT::Periodic::Unattended-Upgrade "0";```

## Tweak OpenSSH Config

Edit ```/etc/ssh/sshd_config```

Make sure the following are set and/or adjusted.

- ```PermitRootLogin no```

Restart the service

```systemctl restart sshd```

## Networking

See ```network_manager.md``` for details on getting online after running the above commands for how to get online and configure routing.

# Searx

Self hosted metasearch. Prevent profiling by major search engines

## Inspiration / Further Reading

- [https://asciimoo.github.io/searx/](https://asciimoo.github.io/searx/)
- [https://github.com/asciimoo/morty](https://github.com/asciimoo/morty)
- [https://asciimoo.github.io/searx/user/own-instance.html](https://asciimoo.github.io/searx/user/own-instance.html)

## Install / Update / Run Script

Setup a generic script that'll auto update Searx, build a container and launch it. You should only run this script at first launch and/or when you're looking for updates.

``` bash

mkdir -p /var/searx
chown root:root /var/searx
mkdir -p /root/docker/searx
git clone https://github.com/asciimoo/searx.git /root/docker/searx/src

cat > /root/docker/searx/searx.sh << EOF
#!/bin/bash

cd /root/docker/searx/src
git checkout Dockerfile
git fetch
LATESTTAG=\`git describe --abbrev=0 --tags\`
git checkout \$LATESTTAG

ARCH=\`arch\`

# Cleanup arch/container image here
if [ \$ARCH == "aarch64" ]
then
    echo "64bit arm"
    sed -i 's_alpine:3.5_arm64v8/alpine:3.5_g' Dockerfile
else
    echo "32bit arm"
    sed -i 's_alpine:3.5_arm32v6/alpine:3.5_g' Dockerfile
fi

docker build \\
    --network docker-private \\
    --file ./Dockerfile \\
    --tag searx/searx:\$LATESTTAG \\
    .

# Cleanup existing container
docker stop searx
docker rm searx

# Re-run/create container with latest image
docker run \\
    --name searx \\
    --restart unless-stopped \\
    --net docker-private \\
    --ip 172.30.8.8 \\
    -e TZ=UTC \\
    -e DEBUG=1 \\
    -e BASE_URL=searx.domain.tld \\
    searx/searx:\$LATESTTAG
EOF

chmod a+x /root/docker/searx/searx.sh

```

## Run Searx

Simply execute ```/root/docker/searx/searx.sh``` to update/run Gogs.

## Serving Via Caddy

``` bash

cat > /etc/caddy/services/searx.conf <<EOF
# Searx proxy
searx:80, searx:443, searx.domain.tld:80, searx.domain.tld:443 {
    redir 301 {
        if {scheme} is http
        /  https://searx.domain.tld{uri}
    }

    log /var/log/caddy/searx.log
    proxy / 172.30.8.8:8888 {
        transparent
    }

    # Use acme.sh Let's Encrypt SSL cert setup
    tls /var/acme.sh/domain.tld/fullchain.cer /var/acme.sh/domain.tld/domain.tld.key
}
EOF

```

## Update Unbound

``` bash

cat > /etc/unbound/local_zone/searx.conf <<EOF
local-data: "searx-insecure A 172.30.8.8"
local-data-ptr: "172.30.8.8 searx-insecure"
local-data: "searx-insecure.domain.tld A 172.30.8.8"
local-data-ptr: "172.30.8.8 searx-insecure.domain.tld"

local-data: "searx A 172.30.0.1"
local-data-ptr: "172.30.0.1 searx"
local-data: "searx.domain.tld A 172.30.0.1"
local-data-ptr: "172.30.0.1 searx.domain.tld"
EOF

```

# Private Internet Access (PIA)

```TODO : INCOMPLETE```

This is a **VERY** advanced topic with some creative tricks to simplify getting the config added to NetworkManager. You're on your own.

The author *strongly* recommends reading through this and adapting to other services well ahead of any real need(s).

## Inspiration / Sources

- [http://blog.deadlypenguin.com/blog/2017/04/24/vpn-auto-connect-command-line/](http://blog.deadlypenguin.com/blog/2017/04/24/vpn-auto-connect-command-line/)
- [https://forums.linuxmint.com/viewtopic.php?t=97187](https://forums.linuxmint.com/viewtopic.php?t=97187)
- [https://unix.stackexchange.com/questions/301845/scripting-a-way-to-quickly-import-ovpn-files-to-networkmanager-on-ubuntu](https://unix.stackexchange.com/questions/301845/scripting-a-way-to-quickly-import-ovpn-files-to-networkmanager-on-ubuntu)
- [https://www.privateinternetaccess.com/pages/client-support/](https://www.privateinternetaccess.com/pages/client-support/)

## Pep Work

Download PIA OpenVPN advanced configs, drop them in a good place on the filesystem and script import.

All files can be found Under ```Advanced OpenVPN SSL Restrictive Configurations``` on the main PIA website.

### Download PIA OpenVPN Template Files

``` bash

mkdir -p /etc/pia
cd /etc/pia
mkdir openvpn-strong
cd openvpn-strong
wget https://www.privateinternetaccess.com/openvpn/openvpn-strong.zip
unzip openvpn-strong.zip
cd ..
mkdir openvpn-strong-tcp
cd openvpn-strong-tcp
wget https://www.privateinternetaccess.com/openvpn/openvpn-strong-tcp.zip
unzip openvpn-strong-tcp.zip
cd ..

```

### Add OpenVPN To Network Manager

``` bash

# NetworkManager Gnome is *required* to get a missing library on xenial
# OMIT Gnome item if you can get away with it...

apt update
apt install network-manager-openvpn network-manager-openvpn-gnome

```

### Cleanup Files for Import

The cipher lines in the templates provided by PIA won't work 100% correctly on import. These commands will clean them up for import.

``` bash

cd /etc/pia
cd openvpn-strong
sed -i 's/aes-256-cbc/AES-256-CBC/g' *.ovpn
sed -i 's/sha256/SHA256/g' *.ovpn
sed -i 's_crl.rsa.4096.pem_/etc/pia/openvpn-strong/crl.rsa.4096.pem_g' *.ovpn
sed -i 's_ca.rsa.4096.crt_/etc/pia/openvpn-strong/ca.rsa.4096.crt_g' *.ovpn
cd ..
cd openvpn-strong-tcp
sed -i 's/aes-256-cbc/AES-256-CBC/g' *.ovpn
sed -i 's/sha256/SHA256/g' *.ovpn
sed -i 's_crl.rsa.4096.pem_/etc/pia/openvpn-strong-tcp/crl.rsa.4096.pem_g' *.ovpn
sed -i 's_ca.rsa.4096.crt_/etc/pia/openvpn-strong-tcp/ca.rsa.4096.crt_g' *.ovpn
cd ..

```

## Import / Setup

### Manual Import

Import the cleaned up, provided OpenVPN profiles. This will require intervention on your part after the profile is created in NetworkManager to ensure it can connect properly.

*Note: this assumes UDP profiles, tweak for the ```openvpn-strong-tcp``` directory if desired or necessary*

#### Initial Import / Edit

``` bash

nmcli connection import type openvpn file /etc/pia/openvpn-strong/[con_name].ovpn
nmcli con show
vim /etc/NetworkManager/system-connections/[con_name]

```
#### Adjustments/Necessary Verification

``` bash

id=PIA - [con_name]

[vpn]
auth=SHA256
cipher=AES-256-CBC
password-flags=0
user-name=[your_username]

[vpn-secrets]
password=[your_password]

```

#### Update Profiles in NetworkManager

``` bash

# Reload For Changes
nmcli connection reload [uuid_from_above]

# Connect
nmcli con up [uuid_from_above]

```

### Scripted Import

Some automated imports based on how the ```ovpn``` files are normally imported via ```nmcli```

*Note: this assumes UDP profiles, you'll need to tweak this as you see fit for TCP*

#### Install ```uuidgen```

``` bash

apt update
apt install uuid-runtime

```

#### Create List of VPN Endpoints

``` bash

cat > /etc/pia/server_list.txt << EOF
us-west.privateinternetaccess.com=PIA - USA (West)
us-east.privateinternetaccess.com=PIA - USA (East)
us-midwest.privateinternetaccess.com=PIA - USA (Midwest)
aus.privateinternetaccess.com=PIA - Australia (Sydney)
austria.privateinternetaccess.com=PIA - Austria
belgium.privateinternetaccess.com=PIA - Belgium
ca-toronto.privateinternetaccess.com=PIA - Canada (Toronto) (East)
ca-vancouver.privateinternetaccess.com=PIA - Canada (Vancouver) (West)
fi.privateinternetaccess.com=PIA - Finland
france.privateinternetaccess.com=PIA - France
germany.privateinternetaccess.com=PIA - Germany
hk.privateinternetaccess.com=PIA - Hong Kong
in.privateinternetaccess.com=PIA - India
japan.privateinternetaccess.com=PIA - Japan
mexico.privateinternetaccess.com=PIA - Mexico
nl.privateinternetaccess.com=PIA - Netherlands
no.privateinternetaccess.com=PIA - Norway
sg.privateinternetaccess.com=PIA - Singapore
spain.privateinternetaccess.com=PIA - Spain
sweden.privateinternetaccess.com=PIA - Sweden
swiss.privateinternetaccess.com=PIA - Switzerland
turkey.privateinternetaccess.com=PIA - Turkey
uk-london.privateinternetaccess.com=PIA - UK (London)
brazil.privateinternetaccess.com=PIA - Brazil
EOF

```

#### Setup NeworkManager Profiles

Some fancy bash tricks to get the full list of NetworkManager PIA connections imported in one copy/paste.

*Note: You'll need to fill in ```[your_username]``` and ```[your_password]``` before running this block of commands.

``` bash

export PIA_USER="[your_username]"
export PIA_PASSWORD="[your_password]"
while read line;
do
desc=$(echo $line | cut -f2 -d'=')
dns=$(echo $line | cut -f1 -d'=')
file="/etc/NetworkManager/system-connections/$desc"

cat > "$file" <<EOF
[connection]
id=$desc
uuid=`uuidgen`
type=vpn
permissions=
secondaries=

[vpn]
connection-type=password
auth=SHA256
password-flags=0
remote=${dns}:1197
cipher=AES-256-CBC
comp-lzo=yes
reneg-seconds=0
remote-cert-tls=server
ca=/etc/pia/openvpn-strong/ca.rsa.4096.crt
dev=tun
service-type=org.freedesktop.NetworkManager.openvpn
username=${PIA_USER}

[vpn-secrets]
password=${PIA_PASSWORD}

[ipv4]
dns-search=
method=auto

[ipv6]
addr-gen-mode=stable-privacy
dns-search=
method=auto

EOF

chmod 600 "$file"

done < /etc/pia/server_list.txt

systemctl restart NetworkManager
nmcli con show

```

## Testing VPN (Post Setup)

Go to each link below and verify you're safe.

- [https://www.privateinternetaccess.com/pages/whats-my-ip/](https://www.privateinternetaccess.com/pages/whats-my-ip/)
- [http://dnsleak.com/](http://dnsleak.com/)
- [http://ipv6leak.com/](http://ipv6leak.com/)
- [http://emailipleak.com/](http://emailipleak.com/)

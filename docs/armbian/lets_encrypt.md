# Let's Encrypt

Use [acme.sh](https://github.com/Neilpang/acme.sh/) for wholly self-contained Let's Encrypt certificates. This assumes CloudFlare DNS is used for authentication.

*Note: You probably want to use a DNS provider/API so you don't have to expose a service to the outside world*

*NOTE: You may want to use a filesystem on a USB disk instead of /var for the volumes setup in the below Docker command(s) to help reduce writes to the micro sd card*

## Dependencies

``` bash
apt update
apt install jq

```

## Prep

Grab the acme.sh Dockerfile and update it to work with arm (32 or 64).

``` bash

mkdir -p /root/docker/acme.sh
cd /root/docker/acme.sh
wget https://raw.githubusercontent.com/Neilpang/acme.sh/master/Dockerfile
sed -i '1s/^/ARG ALPINE=alpine:3.6\n/' Dockerfile
sed -i '/FROM/c\FROM $ALPINE' Dockerfile
mkdir /var/acme.sh
chmod 700 /var/acme.sh

```

## Setup / Run

Setup a basic update/run script with the adjusted upstream Dockerfile

``` bash

cat > /root/docker/acme.sh/acme.sh <<EOF
#!/bin/bash

LATEST=\`docker images --no-trunc acme.sh/acme.sh | awk '{print \$2}' | sort -r | head -n1\`

RELEASE=\`curl -s https://api.github.com/repos/Neilpang/acme.sh/releases/latest | jq -r .tag_name\`

if [ \$RELEASE == \$LATEST ]
then
    echo "Already up to date"
fi

ARCH=\`arch\`
ALPINE=""

# Cleanup arch here
if [ \$ARCH == "aarch64" ]
then
    echo "64bit arm"
    ARCH="arm64"
    ALPINE="arm64v8/alpine"
else
    echo "32bit arm"
    ARCH="arm"
    ALPINE="arm32v6/alpine"
fi

echo "Build parms"
echo "    \${RELEASE}"
echo "    \${ARCH}"
echo "    \${ALPINE}"

echo "Running build"

docker build \\
    --network host \\
    --build-arg ALPINE=\$ALPINE \\
    --file /root/docker/acme.sh/Dockerfile \\
    --tag acme_sh/acme_sh:\$RELEASE \\
    .

echo "Running with latest release"

# Cleanup existing container
docker stop acme_sh
docker rm acme_sh

# Re-run/create container with latest image
# daemon (for cron auto renews)
docker run -itd  \\
    -v "/var/acme.sh":/acme.sh \\
    --net=host \\
    --restart unless-stopped \\
    --name=acme_sh \\
    acme_sh/acme_sh:\$RELEASE daemon

EOF

chmod a+x /root/docker/acme.sh/acme.sh

```

## First Run

Run ```cd /root/docker/acme.sh && /root/docker/acme.sh/acme.sh``` to get the container online. The following commands will get your Let's Encrypt certificates created.

*Note: The above script(s) setup the container to auto-run for auto-renew purposes. If you think you'll miss your renew window, force update the certs*

## Get Help

``` bash

docker exec acme.sh --help

```

## Renewals...

If you're going to be on the go, you may want to force rewewal of your scripts ahead of any travel or longer periods of time away from the internet. The author recommends a simple script at ```/root/update_certs.sh``` or similar that calls the necessary command(s) from below.

## Register a Let's Encrypt Account

Only do this **ONCE**

``` bash

docker exec acme.sh \
    --register-account \
    --staging

```

## Issue Cert (CloudFlare DNS API)

``` bash

docker exec \
    -e CF_Email='[your cloudflare email]' \
    -e CF_Key='[your cloudflare api key]' \
    acme.sh \
    --issue \
    --cert-file /acme.sh/domain.tld/domain.tld.crt \
    --dns dns_cf \
    -d domain.tld \
    -d pi-hole-gui.domain.tld \
    -d syncthing-gui.domain.tld \
    -d nextcloud.domain.tld \
    --staging

```

## Force Renew All Certs (CloudFlare DNS API)

``` bash

docker exec \
    -e CF_Email='[your cloudflare email]' \
    -e CF_Key='[your cloudflare api key]' \
    acme.sh \
    --renew-all \
    --force \
    --dns dns_cf \
    --staging

```

## Revoke Cert

``` bash

docker exec acme.sh \
    --revoke \
    -d domain.tld \
    -d pi-hole-gui.domain.tld \
    -d syncthing-gui.domain.tld \
    -d nextcloud.domain.tld \
    --staging

```

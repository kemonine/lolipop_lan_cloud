# Hardware Notes

Various notes and tidbits that may deserve consideration.

## USB Storage

If you're going to run services like NextCloud, Syncthing and/or Samba you will want to consider what kind of USB disk to use. I'd recommend a [Sandisk Cruzer Fit (link)](https://www.sandisk.com/home/usb-flash/cruzer-fit) or [Sandisk Ultra Fit (link)](https://www.sandisk.com/home/usb-flash/ultra-fit-usb) or similar, low power flash drive. Full hard disks that aren't SSD's tend to be power hungry and can cause problems with SBCs.

## WiFi

- The author has had decent luck with the [ASUS USB-N13 (link)](https://www.asus.com/us/Networking/USB-N13/) adapter in client mode. Be minful of your 2.4ghz channel setup. If this adapter and your AP share a channel in 2.4ghz this adapter *will* drop packets and/or suffer disconnections
  - Sometimes a reboot is necessary to get authentication to work properly after initial setup
  - The author has **NOT** tested AP mode
- The author has had very good luck with the [ASUS USB-N53 (link)](https://www.asus.com/us/Networking/USBN53/) in AP mode. *Both* 2.4ghz and 5ghz can used for AP mode at the same time.
  - The author has **NOT** tested client mode
- The author has had decent luck with the TP-Link N150 USB WiFi adapter in client mode
- The author could not get the TP-Link N300 USB WiFi adapter to work in client mode

## Single Board Computers (SBC)

### Orange Pi Zero

The Orange Pi Zero has buggy WiFi and will **NOT** work for production purposes. However, the rest of the device is functional. Please note the WiFi chip in the Orange Pi Zero is "junk" (per the driver devs) and is unlikely to ever be supported fully.

You'll need/want a USB hub and USB WiFi adapter for this device.

### Pine64

- The 'Euler Bus' has dedicated ```DC IN``` and ```GND``` pins. If you run into power problems, consider these headers for powering the board.
- [Pinout reference (link)](http://files.pine64.org/doc/Pine%20A64%20Schematic/Pine%20A64%20Pin%20Assignment%20160119.pdf)

### Raspberry Pi

The Raspberry Pi devices are notorious for power problems. Especially lack of power for USB devices. You'll want to do a little additional research to ensure you have a good power supply.

## Expansion / Hardware Ideas

### WiFi Antennas

- [https://www.mouser.com/ProductDetail/?qs=%2fv8iy7V9uiwj65CKT%2f%252b6tQ%3d%3d](https://www.mouser.com/ProductDetail/?qs=%2fv8iy7V9uiwj65CKT%2f%252b6tQ%3d%3d)
- [https://www.mouser.com/ProductDetail/?qs=G9o9YCnxvoZVoyw0A06Ktg%3d%3d](https://www.mouser.com/ProductDetail/?qs=G9o9YCnxvoZVoyw0A06Ktg%3d%3d)
- [https://www.mouser.com/ProductDetail/?qs=WUa1z%2fNV9%252b2lzv2ZS%2f50GQ%3d%3d](https://www.mouser.com/ProductDetail/?qs=WUa1z%2fNV9%252b2lzv2ZS%2f50GQ%3d%3d)
- [https://www.mouser.com/ProductDetail/?qs=RuW%2fu%252bNMQmv6yDroBT8RNQ%3d%3d](https://www.mouser.com/ProductDetail/?qs=RuW%2fu%252bNMQmv6yDroBT8RNQ%3d%3d)
- [https://www.mouser.com/ProductDetail/Antenova/SR4W030-100?qs=sGAEpiMZZMuBTKBKvsBmlN73K%2f2BcYXln6YUd9YVZ3FLX3OerI69PA%3d%3d](https://www.mouser.com/ProductDetail/Antenova/SR4W030-100?qs=sGAEpiMZZMuBTKBKvsBmlN73K%2f2BcYXln6YUd9YVZ3FLX3OerI69PA%3d%3d)
- [https://www.mouser.com/ProductDetail/Antenova/SRF2W021-100?qs=sGAEpiMZZMuBTKBKvsBmlMeP1Lut7uca61hspfdOxQexT8ZJsKeXqw%3d%3d](https://www.mouser.com/ProductDetail/Antenova/SRF2W021-100?qs=sGAEpiMZZMuBTKBKvsBmlMeP1Lut7uca61hspfdOxQexT8ZJsKeXqw%3d%3d)

### Power / LiPo Batteries

- [SparkFun mosfet power control (link)](https://www.sparkfun.com/products/12959)
- [Pimoroni On/Off shim (link)](https://www.adafruit.com/product/3581)
- [SparkFun Charger/Booster (link)](https://learn.sparkfun.com/tutorials/sparkfun-5v1a-lipo-chargerbooster-hookup-guide)
- [Power Meter](https://www.sparkfun.com/products/14331)
- [Pimoroni LiPo shim (link)](https://www.adafruit.com/product/3196)

### Storage

- [Ableconn mSATA Hat (link)](https://www.amazon.com/dp/B00WQJ8BH2)
- [Ableconn nvme Hat (link)](https://www.amazon.com/dp/B01LZ0LCTU)

### Displays

- [Waveshare 4.2 inch e-ink display (link)](https://www.waveshare.com/product/modules/oleds-lcds/e-paper/4.2inch-e-paper-module.htm?___SID=U)
- [Waveshare 2.9 inch e-ink display (link)](https://www.waveshare.com/product/modules/oleds-lcds/e-paper/2.9inch-e-paper-module.htm?___SID=U)
- [Pimoroni e-ink hat (link)](https://www.adafruit.com/product/3743)

### Input

- [Waveshare touch keypad (link)](https://www.waveshare.com/product/RPi-Touch-Keypad.htm)
- [Pimoroni touch button hat (link)](https://www.adafruit.com/product/3472)
- [Pimoroni push button shim (link)](https://www.adafruit.com/product/3582)
- [Adafruit display+button hat (link)](https://www.adafruit.com/product/3531)
- [Adafruit joystick+button hat (link)](https://www.adafruit.com/product/3464)
- [Pimoroni display+button hat (link)](https://www.adafruit.com/product/2694)

### Cell Data

- [LTE rPi Shield (link)](http://sixfab.com/product/raspberry-pi-3g-4glte-base-shield-v2/)
- [LTE/4G pciE (link)](http://sixfab.com/product/quectel-ec25-mini-pcle-4glte-module/)
- [3G pciE (link)](http://sixfab.com/product/quectel-uc20-mini-pcle-3g-module/)

### Misc

- [SparkFun TTL serial usb adapter (link)](https://www.sparkfun.com/products/14050)
- [Adafruit TTL serial usb adapter (link)](https://www.adafruit.com/product/3309)
- [SparkFun Bluetooth Mate Silver (link)](https://www.sparkfun.com/products/12576)
- [Adafruit GPS (link)](https://www.adafruit.com/product/746)
- [Pimoroni mini hat extender (link)](https://www.adafruit.com/product/3182)
- [Pimoroni hat extender (link)](https://www.adafruit.com/product/3742)

## 3d Printed Cases

- Pine64 [(source)](https://www.thingiverse.com/thing:1831345) : ```3d_printer_sources/Pine_A64_Two-Parts_case.zip```
- Orange Pi Zero [Plus] Base [(source)](https://www.thingiverse.com/thing:2776831) : ```3d_printer_sources/Orange_Pi_Zero_-_Minimal_Mount.zip```
- Orange Pi Zero [Plus] Case [(source)](https://www.thingiverse.com/thing:1939780) : ```3d_printer_sources/Orange_Pi_Zero_Case.zip```
- Orange Pi Zero [Plus] + Expansion Board Case [(source)](https://www.thingiverse.com/thing:2353879) : ```3d_printer_sources/Orange_Pi_Zero_+_Expansion_Board_Case.zip```
- Orange Pi Zero [Plus] + NAS Board Case [(source)](https://www.thingiverse.com/thing:2122451) : ```3d_printer_sources/Orange_Pi_Zero_NAS_Board_Case.zip```
- Orange Pi PC Case (External Mounts) [(source)](https://www.thingiverse.com/thing:2239240) : ```3d_printer_sources/Orange_PI_PC_Case_with_External_mounts_+_M5_mount.zip```
- Orange Pi PC NAS Case [(source)](https://www.thingiverse.com/thing:2468854) : ```3d_printer_sources/Orange_PI_PC_NAS_Case.zip```
- Orange Pi One Case [(source)](https://www.thingiverse.com/thing:1447933) : ```3d_printer_sources/OrangePi_One_Case.zip```
- Orange Pi One NAS Case [(source)](https://www.thingiverse.com/thing:2790266) : ```3d_printer_sources/Orange_Pi_One_NAS_Case.zip```
- Orange Pi Zero NAS Case (minimal) [(source)](https://www.thingiverse.com/thing:2740032) : ```3d_printer_sources/Orange_Pi_Zero_NAS_minimal.zip```
- Orange Pi Zero 2+ H5 Case [(source)](https://www.thingiverse.com/thing:2797865) : ```3d_printer_sources/Orange_Pi_Zero_2+_H5_Case.zip```
- Orange Pi Zero 2 Case [(source)](https://www.thingiverse.com/thing:2626323) : ```3d_printer_sources/Orange_Pi_Zero_2_Case.zip```
- Orange Pi Plus 2E Case [(source)](https://www.thingiverse.com/thing:2251219) : ```3d_printer_sources/Orange_Pi_plus_2e_case.zip```
- Orange Pi Plus 2E Case (alt) [(source)](https://www.thingiverse.com/thing:1916113) : ```3d_printer_sources/Orange_Pi_Plus_2E_case_2.zip```
- Orange Pi Zero Plus2 H3 Case [(source)](https://www.thingiverse.com/thing:2802598) : ```3d_printer_sources/
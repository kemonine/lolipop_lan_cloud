# GPS

Location fun. Tracking, logging, etc

## Inspiration / Further Reading

- [https://github.com/wadda/gps3](https://github.com/wadda/gps3)
- [https://learn.adafruit.com/adafruit-ultimate-gps-on-the-raspberry-pi?view=all](https://learn.adafruit.com/adafruit-ultimate-gps-on-the-raspberry-pi?view=all)
- [https://learn.adafruit.com/assets/3715](https://learn.adafruit.com/assets/3715)
- [https://learn.adafruit.com/adafruit-ultimate-gps?view=all](https://learn.adafruit.com/adafruit-ultimate-gps?view=all)
- [https://stackoverflow.com/questions/16989153/saving-gps-data-as-gpx](https://stackoverflow.com/questions/16989153/saving-gps-data-as-gpx)
- [http://www.catb.org/gpsd/gpspipe.html](http://www.catb.org/gpsd/gpspipe.html)
- [http://www.catb.org/gpsd/gpsd-time-service-howto.html#_feeding_chrony_from_gpsd](http://www.catb.org/gpsd/gpsd-time-service-howto.html#_feeding_chrony_from_gpsd)
- [http://thomasloughlin.com/gpspipe-gps-client/](http://thomasloughlin.com/gpspipe-gps-client/)

## Hardware

The author chose the [Adafruit Ultimate GPS Breakout (link)](https://www.adafruit.com/product/746) for this guide. YMMV with other boards.

## Setup

``` bash
armbian-config # enable hardware uarts
apt install gpsd gpsd-clients python-gps gpsbabel # necessary software
systemctl stop gpsd # stop gpsd to help with testin
gpsd -n -N -D 2 /dev/ttyS2 # run gpsd by hand
cgps -s # run a cli client to verify it works

```

Setup necessary ```gpsd``` defaults (daemon that provides location data to programs).

Edit ```/etc/default/gpsd```

- Add ```/dev/ttyS2``` to ```DEVICES```
- Add ```-n``` to ```GPSD_OPTIONS```

## Chrony

Add GPS as a time source to chrony

``` bash

cat >> /etc/chrony/chrony.conf <<EOF
# set larger delay to allow the NMEA source to overlap with
# the other sources and avoid the falseticker status
refclock SHM 0 refid GPS precision 1e-1 offset 0.9999 delay 0.2
refclock SOCK /var/run/gpsd.sock refid PPS
EOF

systemctl restart chrony
cgps # look for ```Status:     3D FIX```
chronyc sources # Should see GPS and SOC1 with times for recent samples

```

## GPX Tracks

### NextCloud

**GpxPod** for NextCloud is a good viewer for GPX track recordings

### Tracking

```gpspipe -r -d -l -o /tmp/xxx.nmea``` will record the raw GPS data from the module. Can be used for tracking.

### GPX Files

You can convert raw ```nmea``` data from ```gpspipe``` using ```gpsbabel```

Examples

- ```gpsbabel -t -r -w -i nmea -f /tmp/xxx.nmea -o gpx -F xxx.gpx```
- ```gpsbabel -t -i nmea -f /tmp/xxx.nmea -x track,pack,split=5m,title="LOG # %Y%m%d-%H%M" -o gpx -F out.gpx```

## Automatic Logging

The Python program used here is in the ```scripts``` directory.

``` bash

mkdir -p /tank/gps_data
chown -R gpsd:root /tank/gps_data
# put gpslog.py at /usr/local/bin
cat > /etc/systemd/system/gps-logger.service <<EOF
[Unit]
Description=Simple gps logger
After=gpsd.service gpsd.socket

[Service]
User=gpsd
ExecStart=/usr/local/bin/gpslog.py
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable gps-logger
systemctl start gps-logger

```

## Setup Auto GPX Track Creation/Upload

A handy trick for auto-converting your GPS log data as well as uploading it to NextCloud. The Python program used here is in the ```scripts``` directory.

``` bash

cat > /etc/systemd/system/gpsconvert.service <<EOF
[Unit]
Description=Merge GPS logs and upload to NextCloud
After=gps-logger

[Service]
ExecStart=/usr/local/bin/gpsconvert.py
EOF

cat > /etc/systemd/system/gpsconvert.timer <<EOF
[Unit]
Description=Run GPS log merge

[Timer]
OnCalendar=hourly
Persistent=true

[Install]
WantedBy=timers.target
EOF

systemctl daemon-reload
systemctl enable gpsconvert.timer
systemctl start gpsconvert.timer

```

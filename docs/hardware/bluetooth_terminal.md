# Bluetooth Serial Console

Setup a cheap, simple bluetooth adapter as a serial console.

**THIS IS VERY ADVANCED. YOU PROBABLY DON'T WANT TO DO THIS UNLESS YOU'RE WILLING TO PLAY WITH HARDWARE**

## Inspiration / Further Reading

- [https://www.sparkfun.com/products/12576](https://www.sparkfun.com/products/12576)
- [https://learn.sparkfun.com/tutorials/using-the-bluesmirf](https://learn.sparkfun.com/tutorials/using-the-bluesmirf)
- [https://cdn.sparkfun.com/assets/1/e/e/5/d/5217b297757b7fd3748b4567.pdf](https://cdn.sparkfun.com/assets/1/e/e/5/d/5217b297757b7fd3748b4567.pdf)
- [https://www.sparkfun.com/datasheets/Wireless/Bluetooth/rn-42-ds.pdf](https://www.sparkfun.com/datasheets/Wireless/Bluetooth/rn-42-ds.pdf)
- [https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal](https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal)
- [https://play.google.com/store/apps/details?id=com.ucconnect.ucbtadapter_hex](https://play.google.com/store/apps/details?id=com.ucconnect.ucbtadapter_hex)
- [https://blog.miguelgrinberg.com/post/a-cheap-bluetooth-serial-port-for-your-raspberry-pi](https://blog.miguelgrinberg.com/post/a-cheap-bluetooth-serial-port-for-your-raspberry-pi)
- [https://linux-sunxi.org/Xunlong_Orange_Pi_Zero](https://linux-sunxi.org/Xunlong_Orange_Pi_Zero)
- [https://unix.stackexchange.com/questions/378359/how-to-use-uart-pins-on-orange-pi-zero](https://unix.stackexchange.com/questions/378359/how-to-use-uart-pins-on-orange-pi-zero)

## Hardware

This document assumes the Sparkfun Bluetooth Mate Silver [(link)](https://www.sparkfun.com/products/12576) is used for the bluetooth serial console.

## Android Apps

Connect to your device on the go

- [https://f-droid.org/packages/ru.sash0k.bluetooth_terminal/](https://f-droid.org/packages/ru.sash0k.bluetooth_terminal/)
- [https://play.google.com/store/apps/details?id=ru.sash0k.bluetooth_terminal](https://play.google.com/store/apps/details?id=ru.sash0k.bluetooth_terminal)
- [https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal](https://play.google.com/store/apps/details?id=de.kai_morich.serial_bluetooth_terminal)

## Hardware Connection

Connect the Bluetooth Mate Silver to the Orange Pi Zero UART 1 or 2 on expansion header.

*Note: use UART1 if possible. It's more likely to be present already*

| BT Mate Silver Pin | UART 1 Pin | UART 2 Pin |
| ------------------ | :--------: | :--------: |
| CTS                | N/A        | 15         |
| RTS                | N/A        | 22         |
| Rx                 | 8          | 13         |
| Tx                 | 10         | 11         |
| VCC                | 1          | 1          |
| GND                | 9          | 9          |

## Setup Orange Pi UART

Edit ```/boot/armbianEnv.txt``` and add ```uart1``` to the ```overlays``` line.

Reboot after applying the changes

## Configure Bluetooth Mate Silver

``` bash

minicom -s

```

The below commands should be run to configure the Bluetooth Mate Silver. You'll need to adapt these to your needs.

``` text
$$$ # Enter command mode
V # Display firmware version
D # Displays current config
SF,1 # Restore to factory defaults
ST,0 # Disable remote configuration
SR,Z # Erase any stored addresses
SU,115K # Set BT serial speed
SA,4 # Force entering a pin code to connect
SE,1 # Turn on crypo if firmware below 5.40 (on by default otherwise)
SM,<value> # sets the mode (6 pairing)
SN,<string(max 20 chars)> # Set device name
SP,1234 # Set pin code (up to 20 alphanumeric)
SY,0000 # Set a lower antenna power mode (0 in this case, FFF4 for -12, FFF8 for -8, FFFC for -4)
S|,0A03 # Cycle on for 3 seconds and off for 10 seconds (adjust per how fiddly your bt is to connect)
R,1 # Reboot device
--- # Exit command mode
```

## Enable New Serial tty

Start / enable a new serial tty with the following

``` bash

# Adjust the default serial-getty config for the BT device/oPi
cat > /etc/systemd/system/serial-getty@ttyS1.service <<EOF
#  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.

[Unit]
Description=Serial Getty on %I
Documentation=man:agetty(8) man:systemd-getty-generator(8)
Documentation=http://0pointer.de/blog/projects/serial-console.html
BindsTo=dev-%i.device
After=dev-%i.device systemd-user-sessions.service plymouth-quit-wait.service
After=rc-local.service

# If additional gettys are spawned during boot then we should make
# sure that this is synchronized before getty.target, even though
# getty.target didn't actually pull it in.
Before=getty.target
IgnoreOnIsolate=yes

[Service]
ExecStart=-/sbin/agetty 115200 %I $TERM
Type=idle
Restart=always
UtmpIdentifier=%I
TTYPath=/dev/%I
TTYReset=yes
TTYVHangup=no
KillMode=process
IgnoreSIGPIPE=no
SendSIGHUP=no

[Install]
WantedBy=getty.target
EOF

ln -s /etc/systemd/system/serial-getty@ttyS1.service /etc/systemd/system/getty.target.wants
systemctl start serial-getty@ttyS1
systemctl enable serial-getty@ttyS1

```

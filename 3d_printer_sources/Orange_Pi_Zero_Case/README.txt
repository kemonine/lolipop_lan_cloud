                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1939780
Orange Pi Zero Case by wywywywy is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

Update 7th Mar 2017 - Uploaded the Fusion 360 source files if you want to modify it.

Summary
=======

This is a case for the [Orange Pi Zero](http://s.click.aliexpress.com/e/6YvB2f6), a super low cost (US$7) mini computer like the Raspberry Pi Zero, but with built-in wired & wifi networking and more processing power.  Perfect for using with Octoprint to control 3D printers.

Features
-----

- Slim design
- Vent opening
- Multiple options
- Easy print
- Cut-out for the pin header (for adding extra USB ports etc)

The Base
-------

There are two options - one just the base model, one with a mounting ear (perfect for 2020 aluminium profiles).

There is a 3mm hole for fitting the antenna through.

Mount the board with four M3 6mm screws.

The Cover
-----

There are two options - one with slot vent, one with honeycomb vent.  They are otherwise the same.

The cover is friction fitted to the base with no screws needed, and is quite snug.

Printing
-----

The STL files must be rotated before printing, with the flat side down.

Those in the pictures are printed on a Wanhao Duplicator i3, in FormFutura HD-glass see-through black PETG, with a 0.4mm nozzle, 0.28mm layer height, 3 top/bottom layers, and 3 perimetres.
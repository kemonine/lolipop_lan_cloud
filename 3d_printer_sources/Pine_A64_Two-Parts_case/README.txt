                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1831345
Pine A64 Two-Parts case by harlandraka is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

**WARNING**
Cases with fan support have been removed, the position was wrong and didn't match the CPU.
I will add them again later.



 So this is basically the Pine Brick Case without the sliding element. Easier to print, easy to mount.

Parts needed to close the case:
  - 4xM3 screws, 29mm long
  - 4xM3 nuts

Print with supports **touching buildplate**.

Files description:
  - bottom.stl: The bottom of the case
  - top.stl: The top of the case, with no additional holes (as shown in the pictures)
  - top_buttons.stl: The top of the case, with holes for power/reset buttons
  - top_GPIO_EULER.stl: The top of the case, with holes for GPIO and EULER bus
  - top_buttons_GPIO_EULER.stl: The top of the case, with holes for power/reset buttons, GPIO and EULER bus
  - top_30x30fan.stl: The top of the case, with holes for a 30x30mm fan.
  - top_30x30fan_buttons.stl: The top of the case, with holes for a 30x30mm fan and power/reset buttons.
  - top_30x30fan_buttons_GPIO_EULER.stl: The top of the case, with holes for a 30x30mm fan, power/reset buttons, GPIO and EULER bus.
  - top_30x30fan_GPIO_EULER.stl: The top of the case, with holes for a 30x30mm fan, GPIO and EULER bus.
  - top_40x40fan.stl: The top of the case, with holes for a 40x40mm fan.
  - top_40x40fan_buttons.stl: The top of the case, with holes for a 40x40mm fan and power/reset buttons.
  - top_40x40fan_buttons_GPIO_EULER.stl: The top of the case, with holes for a 40x40mm fan, power/reset buttons, GPIO and EULER bus.
  - top_40x40fan_GPIO_EULER.stl: The top of the case, with holes for a 40x40mm fan, GPIO and EULER bus.

Enjoy!

# Print Settings

Printer: Wanhao Duplicator i3
Rafts: No
Supports: Yes
Resolution: 0.2
Infill: 30%

Notes: 
Supports **touching buildplate**
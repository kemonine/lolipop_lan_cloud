                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2468854
Orange PI PC NAS Case by WarHawk8080 is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

Imported the original designs into tinkercad, after much slicing, cutting, rearranging, pasting, and joining...you see the result

I recommend a [USB to SATA Portable Adapter](https://www.amazon.com/Anker-SATA-Portable-Adapter-Supports/dp/B014OSN2VW/ref=pd_day0_147_4?_encoding=UTF8&pd_rd_i=B014OSN2VW&pd_rd_r=GYWMKNTAWE3C3BWTJXM5&pd_rd_w=gQ6Sq&pd_rd_wg=Il4ab&psc=1&refRID=GYWMKNTAWE3C3BWTJXM5) it can be 2.0 or 3.0 mainly because the choke point for data flow will always be the 100Mb/s transfer thru the network connection

I recommend [DietPi](http://dietpi.com) as the OS, but [Armbian](https://www.armbian.com/) is very very good as well

I plan on using one of the packages in DietPi to setup a personal headless "cloud" server to be able to upload photos remotely to save space on my phone, also could be a NAS or whatever

-Update 08/07/17 found I had reversed the daggum layer..the USB and the Ethernet were on the wrong side for the opening of the harddrive, corrected.  Added a vent from the OPi PC to the harddrive for airflow...the OPi PC fits too snugly into the case lid and would not allow airflow from the 40mm fan to the HD space.

-Update -FINAL- 08/19/17 the top had a tiny cutout in the air vent...corrected and uploaded the top fixed and full fixed.  Printed, very tight fit with PLA, if printed in ABS may need to scale up X and Y by maybe 101% (or more depending on shrinkage)
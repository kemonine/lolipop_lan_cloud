                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2353879
Orange Pi Zero + Expansion Board Case by wywywywy is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

Update 25th Sept 2017 - Source file in Fusion 360 format has been uploaded.

Summary
=======

This is a case for [the Orange Pi Zero with the expansion board module](http://s.click.aliexpress.com/e/6YvB2f6), a super low cost (US$7) mini computer like the Raspberry Pi Zero, but with built-in wired & wifi networking and more processing power.  Perfect for using with Octoprint to control 3D printers.

For a case without the expansion board module, [see my other design.](https://www.thingiverse.com/thing:1939780)

Note that this is not for the Plus model.

Features
-----

- Slim design
- Vent opening
- Multiple options
- Easy print

The Base
-------

There are two options - one just the base model, one with a mounting ear (perfect for 2020 aluminium profiles).

There is a 3mm hole for fitting the antenna through.

You can mount the board with four M3 6mm screws, but it's not necessary because the case holds the boards in place quite well.

The Cover
-----

There are two options - one with slot vent, one with honeycomb vent.  They are otherwise the same.

The cover is friction fitted to the base with no screws needed, and is quite snug.

Printing
-----

Those in the pictures are printed on a VORON CoreXY, in Filamentive black recycled PLA, with a 0.6mm nozzle, 0.28mm layer height, 3 top/bottom layers, and 3 perimeters.
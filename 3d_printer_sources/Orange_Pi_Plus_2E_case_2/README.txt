                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1916113
Orange Pi Plus 2E case by Galane is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

The first case on Thingiverse for the Orange Pi Plus 2E. I printed it and the Orange Pi fit in. Had to raise the height of the hole for the IR sensor. Added vent slots and some other small adjustments.

The top is basic, plain and solid. You'll want to modify it to accommodate things like cooling fans and expansion boards. The Plus 2E runs its quad core H3 CPU rather hot and should have a heat sink. To provide blocking to hold the PCB down, add pegs to the underside of the lid, put plastic sleeves over the bolts or use extra nuts on the bolts. There's not enough clearance to various components on the top side of the board for full 3D printed tubes around the bolts.

I made the SD card opening large enough to run the camera connector through, though using a camera will make inserting and removing cards more difficult, and as the case is will require connecting the camera cable before putting the board in.

The buttons part fits through the holes at the power end. I had to make a recess and bump out to fit the button piece in.

The hole for the AV jack is large enough it can be tipped in. I added a tapered recess around its outside.

# How I Designed This

I have an Orange Pi Plus 2E and took a lot of measurements. The mounting holes are correctly positioned, verified by printing a thing slab with the 3mm holes to compare to the computer.
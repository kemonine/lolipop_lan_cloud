# The Orange Lolipop

An Armbian based portable LAN and Private Cloud based on SBC's like the Orange Pi and Raspberry Pi.

This should be adaptable to any Armbian device but the focus of the documentation is on the Orange Pi board(s) which the author uses for development.

## The Info Dump

### The Basics

- [Hardware Notes](docs/hardware_notes.md)
- [Pre Flight](docs/preflight.md)
- [First Boot](docs/armbian/first_boot.md)
- [Base Setup](docs/armbian/base_setup.md)

### Networking

- [Network Manager (networking setup)](docs/armbian/network_manager.md)
- [Modem Manager (3G/LTE modems)](docs/armbian/modem_manager.md)
- [FirewallD (firewall/routing)](docs/armbian/firewalld.md)
- [Unbound (Non ISP DNS)](docs/armbian/unbound.md)
- [Private Internet Access (VPN)](docs/armbian/pia.md)
- [vpn.ac (VPN)](docs/armbian/vpn_ac.md)
- [VPN Autoconnect](docs/armbian/vpn_autoconnect.md)

### Fundamentals

- [Chrony (time sync)](docs/armbian/chrony.md)
- [Cockpit (basic web ui management console)](docs/armbian/cockpit.md)
- [borg Backups (simple, effective, efficient backups)](docs/armbian/borg.md)
- [Docker (containerize your services)](docs/armbian/docker.md)
- [Let's Encrypt (SSL/TLS certificates)](docs/armbian/lets_encrypt.md)
- [Incron (inotify based cron - handy for automatic service reloads)](docs/armbian/incron.md)
- [Caddy (web server/proxy)](docs/armbian/caddy.md)
- [Pi Hole (Ad Blocking)](docs/armbian/pi_hole.md)
- [Searx (meta search engine)](docs/armbian/searx.md)

### Services

- [Monitoring (basic monitoring via munin)](docs/armbian/monitoring.md)
- [ElasticBeats (Logs/Monitoring [Incomplete])](docs/armbian/elasticbeats.md)
- [Syncthing (large/bulk file sync [when nextcloud isn't appropriate])](docs/armbian/syncthing.md)
- [Postgresl (Use for NextCloud/Gogs/Wallabag/TT-RSS/Turtl if desired, required for some)](docs/armbian/postgres.md)
- [NextCloud (fundamental price cloud services)](docs/armbian/nextcloud.md)
- [Gogs (self hosted git/github/gitlab)](docs/armbian/gogs.md)
- [Wallabag (self hosted read it later / pocket)](docs/armbian/wallabag.md)
- [TT-RSS (self hosted RSS reader - think Google Reader)](docs/armbian/ttrss.md)
- [Turtl (self hosted evernote/one note)](docs/armbian/turtl.md)

### Additional Hardware

- [GPS](docs/hardware/gps.md)
- [RTC (Real Time Clock)](docs/hardware/rtc.md)
- [Bluetooth Serial Terminal](docs/hardware/bluetooth_terminal.md)

## To Do / Future

See [TODO.md](TODO.md) for more information.

## License

See [LICENSE.md](LICENSE.md) for more information.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md) for more information.
